\chapter*{Acknowledgements}

I would like to express my gratitude to my supervisor at \emph{Maison de la Simulation},
M. Samuel \textsc{Kokh}, for all his patience and guidance during this project.
His aid in fully understanding the problem I was facing and the multiple facets
that it presents, as well as his input concerning the implementation, have been
invaluable. I would also like to thank my sister for looking over the report
and finding those little missing \emph{commas} and pointing out the last stray
typos.

Last, but not least, I would like to thank the staff at \emph{Maison de la Simulation}
for making me feel welcome at all times. Especially that one guy who, on my
first day there, asked me to join everybody for a coffee break and made everything
feel less awkward.

Thank you.

\chapter{Introduction}

In this paper we will look at the construction of an anti-dissipative and
maximum preserving numerical scheme called \textbf{Vofire}, from the French
\emph{Volumes finis avec Reconstruction}, as presented in~\cite{lagoutiere2010}.

In particular, we will study the scheme in the context of a 2D transport
equation with a velocity which is not divergence free. This is a departure
from the results offered in~\cite{lagoutiere2010}, which supposes a divergence-free
velocity for the sake of simplicity.

The scheme builds on the understanding we have of anti-diffusive behaviour
in one dimension. In that simple case, diffusion can only occur in the direction
of the velocity, as we will see in the context of the classical upwind and
downwind scheme. In two or more dimensions, we can have multiple directions
in which diffusion can occur. For the 2D example we will be looking at, we have
the longitudinal direction (in the direction of the velocity) and the
transverse direction. Given these two types of diffusion, the construction of
the scheme involves two separate steps that intend to limit the diffusion in
both directions.

We will, however, not be looking at other finite volume schemes with similar
anti-dissipative properties, such as the \emph{VOF}~\cite{vofnichols} family of
methods, the \emph{MUSCL}-type schemes~\cite[p.~426]{toro} or various limiters
(especially \emph{ULTRABEE}~\cite[p.~456]{toro}).

\subsubsection{Projet fin d'études}
This project is achieved during the \emph{Projet fin d'études} that takes
place in the first semester of the third year at the engineering school
\emph{Institut Sup' Galilée}. For a period of 3-4 months, the PFE has a
dedicated day of the week allocated to it in which students have to work on
their projects. These projects are usually done within a company or some
other external research institute. In this case, the project was done as part
of a bigger effort at \textbf{Maison de la Simulation}~\cite{mdlswebsite}.

\subsubsection{Maison de la Simulation}

Maison de la Simulation is a joint research facility between \textbf{CEA}
(the \emph{Centre d'énergie atomique}), \textbf{CNRS} (the \emph{Centre national de la
recherche scientifique}), \textbf{INRIA} (\emph{Institut national de recherche
en informatique et en automatique}) with the involvement of two universities:
\textbf{Université Paris-Sud} and \textbf{Université de Versailles}. The laboratory
was created in 2012 in the DigiteoLabs building in Saclay.

The main focus of the laboratory is promoting \textbf{High Performance Computing}
in France and internationally as part of a bigger effort to fully understand
and make use of emerging technologies and advancing computing power.

In addition to being a research centre, Maison de la Simulation is also an
\emph{expertise centre} open to the scientific community. As part of this goal,
there is an ongoing effort to educate both existing engineers and students
to efficiently use the computing infrastructure available to them. Given this
direction, Maison de la Simulation has a small number of permanent
staff with most of the personnel consisting of temporary staff from various
research institutions and PhD students.

As a hub for HPC in France, Maison de la Simulation has worked on
various projects that involved either optimising existing code (e.g. for
\emph{Gysela5D}), fully developing new applications (e.g. \emph{Ramses-GPU})
or developing novel techniques (e.g. \emph{a new well-balanced scheme for
gravitational flows}).

\subsubsection{Structure}

The paper is structured into 5 chapters, including this one.
\hyperref[ch:two]{Chapter 2} will deal with the mathematical angle, presenting
the construction and design principles of the \emph{Vofire} scheme. In
\hyperref[ch:three]{Chapter 3} we will look at the implementation of the scheme
in \texttt{C} by using a specialized finite volume library called \emph{YAFiVoC}.
In \hyperref[ch:four]{Chapter 4}, we will look at some numerical results
obtained by our implementation, compare them to the results obtained
in~\cite{lagoutiere2010} and look at how the scheme performs under different
conditions.

In the \hyperref[ch:five]{final chapter}, we will try and draw some conclusions
regarding the experience gathered during this project and the performance of the
\emph{Vofire} scheme under different circumstances.
