\chapter{Numerical Results} \label{ch:four}

In this chapter we will present numerical tests that we have performed with
the numerical schemes that were implemented during this internship: the upwind
scheme, the reconstructed flux scheme and the Vofire scheme. We shall compare
the results obtained with these schemes and also the influence of the CFL choice
upon the simulation results.

\section{First Test: Advection of Pacman}

We start with a two-dimensional advection test of a characteristic function
whose mapping at $t = 0$ represents the famous Pacman chasing a ghost (Clyde).
We considered the domain $\Omega = [-1.5, 10.5] \times [-1.5, 1.5]$ that
is discretized over a $600 \times 130$ Cartesian grid. In Figure~\ref{fig:pacmanmesh},
we can see an example grid, with a much smaller number of cells: $100 \times 20$,
over the same domain.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{img/test01/pacmanmesh}
    \caption{Test 1: Pacman. Cartesian mesh with $100 \times 20$ cells.}
    \label{fig:pacmanmesh}
\end{figure}

The initial condition is given by:
\[
c(0, \mathbf{x}) = \ind{\textrm{PACMAN}}(\mathbf{x}) + \ind{\textrm{CLYDE}}(\mathbf{x}) + 1.
\]
where:
\[
\textrm{PACMAN} = \set{(x, y) \in \R^2}{ x^2 + (y - 0.2)^2 < 0.8^2 \text{ and } \pm (y - 0.2) > x}
\]
and:
\[
\begin{aligned}
\textrm{CLYDE} & = \set{(x, y) \in \R^2}{(x - 2.5)^2 + (y - 0.4)^2 < 0.7^2 \text{ and } y > 0} \bigcup \\
&\ \ \ \set{(x, y) \in \R^2}{x \in [1.8, 3.2], y \in [-0.7, 0.5] \text{ and } y > \pm (x - 0.2)}.
\end{aligned}
\]

Furthermore, this test uses periodic boundary conditions, that is to say:
\[
\begin{cases}
c(t, x = -1.5, y) = c(t, x = 10.5, y), \quad \text{for } y \in [-1.5, 1.5] \\
c(t, x, y = -1.5) = c(t, x, y = 1.5), \quad \text{for } x \in [-1.5, 10.5].
\end{cases}
\]

The velocity field is $\mathbf{u}(t, \mathbf{x}) = (1, 0)$, for $(t, x) \in
[0, T] \times \Omega$. For this test, the CFL was set by choosing
$\theta = 0.5$ and the final time is $T = 12.312$.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{img/test01/pacman01}
    \caption{Test 1: Pacman. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 0$.}
    \label{fig:pacman01}
\end{figure}

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{img/test01/pacman02}
    \caption{Test 1: Pacman. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 6.156$.}
    \label{fig:pacman02}
\end{figure}

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{img/test01/pacman03}
    \caption{Test 1: Pacman. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 12.312$.}
    \label{fig:pacman03}
\end{figure}

Figure~\ref{fig:pacman01},~\ref{fig:pacman02} and~\ref{fig:pacman03} display
the profile of the respective solutions at $t \in\{ 0, 6.156, 12.312\}$.
The scheme (in these conditions) is indeed very anti-dissipative: the profile
of the solution at the final instant agrees very well with the initial
condition. Furthermore, let us mention that the numerical results match the
building process of the scheme stability-wise: the simulation result agrees
with the maximum principle.

On the other hand, we have the results given by the the upwind scheme, which
are very \emph{diffusive}, as expected, in Figure~\ref{fig:pacmanupwind01}
and Figure~\ref{fig:pacmanupwind02}. An interesting phenomenon to notice about these
results is the fact that there is no diffusion in the $y$ direction. This is
because the chosen velocity is aligned to the mesh in the $x$ direction and
there is no reason to find dissipation in other directions.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{img/test01/pacmanupwind01}
    \caption{Test 1: Pacman Upwind. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 6.156$.}
    \label{fig:pacmanupwind01}
\end{figure}

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{img/test01/pacmanupwind02}
    \caption{Test 1: Pacman Upwind. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 12.312$.}
    \label{fig:pacmanupwind02}
\end{figure}

\section{Second Test: Advection of a square}

For a second example, we will look at a triangular mesh with a much simpler
shape to transport.

For this test we will consider the domain $\Omega = [-1, 9] \times [-1, 9]$
discretized over a $39362$-triangle mesh (see Figure~\ref{fig:squaremesh}
for an example mesh).

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.3\linewidth]{img/test02/squaremesh}
    \caption{Test 2: Square. Triangular mesh with $4042$ cells.}
    \label{fig:squaremesh}
\end{figure}

The initial data is the characteristic function of the square $[0.8, 3.8] \times [0.8, 3.8]$:
\[
c(0, \mathbf{x}) = \ind{[0.8, 3.8] \times [0.8, 3.8]}(\mathbf{x}) + 1
\]
with Neumann boundary conditions:
\[
\dpd[n]{c}(t, x) = 0, \quad \forall x \in \partial\Omega.
\]

The velocity field is given by:
\[
\mathbf{u} =
\left\{
\begin{array}{LL}
(\sqrt{2}, \sqrt{3}),   & \quad (t, x) \in [0, \sfrac{T}{2}] \times \Omega \\
(-\sqrt{2}, -\sqrt{3}), & \quad (t, x) \in [\sfrac{T}{2}, T] \times \Omega
\end{array}
\right.
\]
This definition ensures that we will get the same profile at $t = 0$ as at
$t = T$ and eases the analysis of the anti-dissipativity of the scheme.

Furthermore, we have used a final time $T = 3.3$ and forced the CFL condition
by choosing $\theta = 0.5$, as in the previous example.

\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test02/square01}
        \caption{Test 2: Square. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 0$.}
        \label{fig:square01}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test02/square02}
        \caption{Test 2: Square. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 3.3$.}
        \label{fig:square02}
    \end{minipage}
\end{figure}

We can see that the square has suffered some modification in this test. This
is due to the fact that the direction of the velocity no longer aligns with
the mesh which in turn produces the spikes we observe in Figure~\ref{fig:square01}.

However, the anti-dissipative mechanism is still very effective as the cells
with numerical diffusion are constrained in a small area at the border of the
transported profile.

As a comparison, in Figure~\ref{fig:squareupwind01} and Figure~\ref{fig:squareupwind02}
we have the same test performed using the \emph{upwind scheme}. The difference
in dissipativity between the two schemes is immediately obvious.

\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test02/squareupwind01}
        \caption{Test 2: Square Upwind. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 1.6$.}
        \label{fig:squareupwind01}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test02/squareupwind02}
        \caption{Test 2: Square Upwind. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 3.3$.}
        \label{fig:squareupwind02}
    \end{minipage}
\end{figure}

\section{Third Test: Solid Body Rotation}\label{sc:solidbody}

For our third test we consider a rotating disk on a triangular mesh (a
solid body rotation). This is test is difficult as it involves a uniform velocity field.

The domain of the test is $\Omega = [-7, 7] \times [-7, 7]$ divided into a
mesh of $30873$ triangles. The initial condition is the characteristic
function of a disk centered in $(3.5, 0)$ with a radius of $1.2$:
\[
c(0, \mathbf{x}) = \ind{\set{(x, y) \in \Omega}{(x - 3.5)^2 + y^2 < 1.2^2}}(\mathbf{x})
\]
with Neumann boundary conditions:
\[
\dpd[n]{c}(t, x) = 0, \quad \forall x \in \partial\Omega.
\]

The velocity we have used is the same as the one provided
in~\cite{lagoutiere2010}: $\mathbf{u} = (-2 \pi y, 2 \pi x)$. Additionally,
we flip the sign of the velocity at $t = \sfrac{T}{2}$, giving:
\[
\mathbf{u} =
\begin{cases}
(-2 \pi y, 2 \pi x), \quad (t, x) \in [0, \sfrac{T}{2}] \times \Omega \\
(2 \pi y, -2 \pi x), \quad (t, x) \in [\sfrac{T}{2}, T] \times \Omega.
\end{cases}
\]

The final time is $T = 1.312$ and the CFL condition is given by $\theta = 0.1$.

\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test03/disk01}
        \caption{Test 3: Disk. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 0$.}
        \label{fig:disk01}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test03/disk02}
        \caption{Test 3: Disk. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 1.3$.}
        \label{fig:disk02}
    \end{minipage}
\end{figure}

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.35\linewidth]{img/test03/diskupwind}
    \caption{Test 3: Disk Upwind. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 0.656$.}
    \label{fig:diskupwind}
\end{figure}

We can see that even in this complex setting, the anti-diffusive mechanism of the scheme performs very well. However,
clear distortions occur at the boundary of the rotated disc even with a very small CFL coefficient.

If we perform the same test using the upwind scheme, we get the results presented
in Figure~\ref{fig:diskupwind}.

\section{Fourth Test: A Continuous Initial Condition}

For this test, we do not consider the transport of a characteristic function.
Let $\Omega = [-3.5, 7.5] \times [-2.5, 2.5]$ be discretized over a Cartesian
grid with $600 \times 130$ cells.

The chosen initial solution is the so-called \emph{Sombrero Function}:
\begin{equation}\label{eq:sombrero}
c(0, \mathbf{x}) = \frac{\sin(5\sqrt{x^2 + y^2})}{5 \sqrt{x^2 + y^2}}
                   \ind{\set{(x, y) \in \Omega}{x^2 + y^2 < 1.5^2}}(\mathbf{x})
\end{equation}
with periodic boundary conditions:
\[
\begin{cases}
c(t, x = -3.5, y) = c(t, x = 7.5, y), \quad \text{for } y \in [-2.5, 2.5] \\
c(t, x, y = -2.5) = c(t, x, y = 2.5), \quad \text{for } x \in [-3.5, 7.5].
\end{cases}
\]

And with a constant velocity field $\mathbf{u} = (1, 0)$. The test is performed
for a final time $T = 12.3$ and a CFL condition imposed by $\theta = 0.5$.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.65\linewidth]{img/test04/sombrero01}
    \caption{Test 4: Sombrero. Graph of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 0$.}
    \label{fig:sombrero01}
\end{figure}

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.65\linewidth]{img/test04/sombrero02}
    \caption{Test 4: Sombrero. Graph of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 0.495$.}
    \label{fig:sombrero02}
\end{figure}

In the case of a continuous function like the one defined by~\eqref{eq:sombrero},
we can see that the scheme seems very inaccurate. Indeed, the profile of the solution seems ``over-compressed''
while it is transported. Even after a very small number of time steps (see Figure~\ref{fig:sombrero02}) the scheme
starts to compress regions of smooth variation of our profile into juxtaposed step functions as we see in
Figure~\ref{fig:sombrero03}.

This feature is a clear flaw of such a scheme.


\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.65\linewidth]{img/test04/sombrero03}
    \caption{Test 4: Sombrero. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 12.3$.}
    \label{fig:sombrero03}
\end{figure}


\begin{figure}[!h]
    \centering
    \begin{minipage}{.46\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test04/sombreroprofile01}
        \caption{Test 4: Sombrero. Profile at $y = 0$ and $t = 0$.}
        \label{fig:sombreroprofile01}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.46\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test04/sombreroprofile02}
        \caption{Test 4: Sombrero. Profile at $y = 0$ and $t = 12.3$.}
        \label{fig:sombreroprofile02}
    \end{minipage}
\end{figure}

To have a more accurate image of the effect of the scheme on a continuous function,
we can look at the profiles of the initial and final states in Figure~\ref{fig:sombreroprofile01}
and Figure~\ref{fig:sombreroprofile02} provided by doing a cut along the $x$-axis
at $y = 0$. Here we can clearly see that at $t = 12.3$, the approximation
is almost completely transformed into step function.

\section{Fifth Test: Time Dependent Velocity}

For our final test, we will look at at time-dependent, non divergence-free
velocity. This test will look at the most substantial departure we have made
from the scheme defined in~\cite{lagoutiere2010}, namely the lack of a
divergence free velocity.

The domain is $\Omega = [-5, 5] \times [-5, 5]$ which has been
discretized over a $220 \times 220$ Cartesian grid.

The initial condition is given by the characteristic function of a circle:
\[
c(t, x) = \ind{\set{(x, y) \in \Omega}{x^2 + y^2 < 1.3^2}}
\]
with homogeneous Neumann boundary conditions:
\[
\dpd[n]{c}(t, x) = 0, \quad \forall x \in \partial\Omega.
\]

The velocity field is given by:
\[
\mathbf{u}(t, \mathbf{x}) = (\sign{(\cos(Ft))}\ x, \sign{(\cos(Ft))}\ y)
\]
where $F$ is a frequency. Visually, the test will perform a pulsation of
a bubble (disk) where $F$ defines the speed at which it will pulsate. In our
test, we have chosen $F = 3$.

Furthermore, we have fixed the final time at $T = 20.0$ with a CFL condition
$\theta = 0.5$.

\begin{figure}[!h]
    \centering
    \begin{minipage}{.47\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test05/pulse01}
        \caption{Test 5: Pulse. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 0$.}
        \label{fig:pulse01}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.47\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test05/pulse02}
        \caption{Test 5: Pulse. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 20.0$.}
        \label{fig:pulse02}
    \end{minipage}
\end{figure}

Besides testing the two new aspects of the scheme: the time dependent and
non-divergence-free velocity, this test is also a \emph{stress test} for our scheme.
We have tried to use a very large value for $T$ so that we can see how the scheme
performs over long periods of time, i.e. whether it keeps its anti-dissipative
nature and to what degree does it transform the solution.

Firstly, the change in velocity with time has not had any detrimental effect on
the anti-dissipative property. We can also see that the scheme is indeed
capable to handle a non-divergence-free velocity, condition that has also
been relaxed in the later part of~\cite{lagoutiere2010} as an extension of the
scheme to \emph{multicomponent Euler equations} in 3D.

Regarding the stress test: it has also been successful. The result at time
$t = T$ still has very little dissipation, but we can see it is also slightly
deformed. We suppose that the deformation the disk has suffered over time is
most likely due to the mesh we are using.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.3\linewidth]{img/test05/pulse03}
    \caption{Test 5: Pulse. Mapping of $\mathbf{x} \mapsto c(t, \mathbf{x})$ at $t = 4.55$.}
    \label{fig:pulse03}
\end{figure}

We can see in Figure~\ref{fig:pulse03} that the deformation happens early on
in the time loop, and it remains constant until the end.

\section{Influence of the Construction Steps}\label{section : influence of the reconstruction steps}

Having seen how the scheme performs on various meshes and with initial conditions
and velocities of various complexity, we will now look at the impact each step
in the construction of the scheme has upon the result. We will therefore look
at the following cases:

\begin{itemize}
    \item $\lambda_{jk} = 0$ and $\mu_{jkr} = 0$, which is the Upwind scheme,
    \item $\lambda_{jk} \neq 0$ and $\mu_{jkr} = 0$,
    \item $\lambda_{jk} = 0$ and $\mu_{jkr} \neq 0$ and
    \item $\lambda_{jk} \neq 0$ and $\mu_{jkr} \neq 0$, which is the final
    Vofire scheme.
\end{itemize}

The test will be performed with the same initial condition and velocity as for
the test of section~\ref{sc:solidbody}, that is
to say the disk in solid body rotation. This test will
try to reproduce the results present in~\cite{lagoutiere2010} regarding the
effect of the various construction steps.

Unlike the article, we will use a quadrangular mesh, to see if it has any
impact on the final result. The mesh will contains $150 \times 150$ cells
over the previously defined domain.

\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.9\linewidth]{img/test06/comparison01}
        \caption{Test 5: Comparison. Upwind Scheme.}
        \label{fig:comparison01}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.9\linewidth]{img/test06/comparison04}
        \caption{Test 5: Comparison. Vofire Scheme.}
        \label{fig:comparison04}
    \end{minipage}
\end{figure}

\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.9\linewidth]{img/test06/comparison02}
        \caption{Test 5: Comparison. $\mu = 0$.}
        \label{fig:comparison02}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.9\linewidth]{img/test06/comparison03}
        \caption{Test 5: Comparison. $\lambda = 0$.}
        \label{fig:comparison03}
    \end{minipage}
\end{figure}

In Figure~\ref{fig:comparison01} we have the Upwind scheme which is very
dissipative, as the 1D results have already shown. Next we can see the Vofire
scheme (in Figure~\ref{fig:comparison04}). The result in this case is slightly
more distorted because of the change in the underlying mesh, but we can
still see that the result is very anti-dissipative.

In Figure~\ref{fig:comparison02} and~\ref{fig:comparison03} we can see
the influence of each of the construction steps of the final flux. In the first
figure, we only perform the \emph{transverse reconstruction} step, which
yields the reconstructed value on each edge. As its name implies, this step
reduces the flux in the transverse direction to the velocity. We can clearly
see this in Figure~\ref{fig:comparison02} where the diffusion is only present
in the direction of the velocity.

In Figure~\ref{fig:comparison03} we only perform the final step in the
construction of our flux, with no \emph{transverse reconstruction}. In this
case, we see that the scheme is anti-dissipative in all directions (due to the
\emph{limiting} nature of this construction step), but there are powerful
oscillations that appear at the border of the disk, rendering the result unusable.

\section{Influence of the CFL Condition}

Finally we will look at the influence of the CFL condition on the scheme. Until
now, we have used CFL coefficients that have been proven to work well in testing,
but as we will see, not all values yield good results.

The test case is performed with the parameters defined in
\hyperref[sc:solidbody]{Third Test: Solid Body Rotation} and with a varying
CFL coefficient $\theta = \{0.1, 0.4, 0.7, 1.0\}$.

\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{img/test07/cfl01}
        \caption{Test 6: CFL Condition. $\theta = 0.1$.}
        \label{fig:cfl01}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{img/test07/cfl02}
        \caption{Test 6: CFL Condition. $\theta = 0.4$.}
        \label{fig:cfl02}
    \end{minipage}
\end{figure}

\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{img/test07/cfl03}
        \caption{Test 6: CFL Condition. $\theta = 0.7$.}
        \label{fig:cfl03}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{img/test07/cfl04}
        \caption{Test 6: CFL Condition. $\theta = 1.0$.}
        \label{fig:cfl04}
    \end{minipage}
\end{figure}

We can see that the CFL condition has a huge impact on the end result of
the simulation. While for a small CFL coefficient we get a satisfying result
(Figure~\ref{fig:cfl01}), for larger values the result is very similar to the result
of section~\ref{section : influence of the reconstruction steps} when we set$\lambda = 0$. Indeed, the
distortions in the direction that is transverse to the transport direction are very important.

However, once again we can observe that the value of the CFL condition does not impact the
anti-dissipative mechanism of the scheme: in each of the 4 figures (\ref{fig:cfl01},
\ref{fig:cfl02}, \ref{fig:cfl03} and \ref{fig:cfl04}) the transported profile remains very sharp despite the deformations.
