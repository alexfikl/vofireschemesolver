TEXMK := "latexmk"
TEXOUTDIR := "latex.out"
TEXFLAGS := "-pdflua -latexoption='-shell-escape -recorder' -output-directory=" + TEXOUTDIR

_default:
    @just --list

[private]
pdf basename:
    @sed -i 's/latexrunfalse/latexruntrue/' {{ basename }}.tex
    {{ TEXMK }} {{ TEXFLAGS }} {{ basename }}.tex
    @sed -i 's/latexruntrue/latexrunfalse/' {{ basename }}.tex
    @cp {{ TEXOUTDIR }}/{{ basename }}.pdf .

[doc("Compile report")]
report:
    @just pdf pfe-report

[doc("Compile presentation")]
presentation:
    @just pdf pfe-presentation

[doc("Create an archive with all the files")]
tar:
    tar -zcvf vofire-docs-`date +"%d-%m-%y"`.tar.gz *.tex *.sty img justfile

[doc("Remove all temporary compilation files")]
clean:
    rm -rf {{ TEXOUTDIR }}
    rm -rf _minted*

[doc("Remove all generated files")]
purge: clean
    rm -rf *.pdf
