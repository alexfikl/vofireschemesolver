\documentclass[xcolor=x11names,compress]{beamer}

%% General document %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage{mymath}
\usepackage{graphicx}
\usepackage{tikz}

%% Beamer Layout %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\useoutertheme[subsection=false,shadow]{miniframes}
\useinnertheme{default}
\usefonttheme{serif}
\usepackage{palatino}

\setbeamerfont{title like}{shape=\scshape}
\setbeamerfont{frametitle}{shape=\scshape}

\setbeamercolor*{lower separation line head}{bg=DeepSkyBlue4}
\setbeamercolor*{normal text}{fg=black,bg=white}
\setbeamercolor*{alerted text}{fg=red}
\setbeamercolor*{example text}{fg=black}
\setbeamercolor*{structure}{fg=black}

\setbeamercolor*{palette tertiary}{fg=black,bg=black!10}
\setbeamercolor*{palette quaternary}{fg=black,bg=black!10}
\setbeamertemplate{navigation symbols}{}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\emph}[1]{\textcolor{DeepSkyBlue4}{#1}}
\setbeamertemplate{caption}{\insertcaption}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\title{\LARGE \textbf{Vofire}\vspace{20pt}}
\subtitle{\Large Un schéma anti-dissipatif pour l'équation de transport
sur des maillages non-structurés}
\author{
    \vspace{-30pt}
}
\date{
    \includegraphics[scale=0.15]{img/logo_cea} \hfill
    \includegraphics[scale=0.2]{img/logo_galilee} \hfill
    \includegraphics[scale=0.15]{img/logo_mdls}
}
\titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Discrétisation volumes finis}
\begin{frame}{L'équation de transport}
On considère l'EDP:
\[
\begin{cases}
\partial_t c + \mathbf{u} \cdot \grad{c} = 0, \quad \text{dans } \Omega \subset \R^2 \\
c(0, \textbf{x}) = c_0 (\mathbf{x})
\end{cases}
\]
où $\mathbf{u}: [0, T] \times \Omega \to \R^2$ est la \emph{vitesse} et
$c: [0, T] \times \Omega \to \R$ est la \emph{quantité transportée}.
\end{frame}

\begin{frame}{Maillage}
Le domaine $\Omega$ est couvert par des éléments $T_j$ \emph{polygonaux}, tels que
$T_i \cap T_j = \{\ \emptyset, \text{ un sommet ou une arête }\}$ et:
\[
\overline{\bigcup_{j \in \llbracket 0, J \rrbracket} T_j} = \overline{\Omega}.
\]

\begin{figure}[!ht]
\begin{center}
\begin{tikzpicture}[scale=0.25]
    % the polygon
    \draw [thick] (-2, 0) -- (4, 0);
    \draw [thick] (4, 0) -- (6, 5);
    \draw [thick] (6, 5) -- (2.5, 9);
    \draw [thick] (2.5, 9) -- (-4, 9);
    \draw [thick] (-4, 9) -- (-7, 4);
    \draw [thick] (-7, 4) -- (-2, 0);

    % the small lines denoting the neighbours
    \draw [thick] (-2, 0) -- (-3.5, -2);
    \draw [thick] (4, 0) -- (4.5, -2);
    \draw [thick] (2.5, 9) -- (4, 11);
    \draw [thick] (6, 5) -- (8, 5.1);
    \draw [thick] (-4, 9) -- (-4.5, 11);
    \draw [thick] (-7, 4) -- (-9, 4);

    % the edge normal
    \draw [->] [thick] [DarkOliveGreen4] (-4.5, 2) -- (-6, 0.2);

    % the edge velocity
    \draw [->] [thick] [DarkOliveGreen4] (-4.5, 2) -- (-2.6, 3.43);

    % the velocity vector
    \draw [->] [thick] [RoyalBlue4] (-12, 7) -- (-4, 13);

    \node at (0, 5) {$T_j$};
    \node at (-10, 0) {$T_k$};
    \node at (-6, -0.5) {$\mathbf{n}_{jk}$};
    \node at (-1.6, 2.9) {$\mathbf{u}^n_{jk}$};
    \node at (-8, 12)  {$\mathbf{u}$};
    \node at (-6.3, 2.5) {$e_{jk}$};
\end{tikzpicture}
\label{fig:meshcell}
\end{center}
\end{figure}
\end{frame}

\begin{frame}{Discrétisation}
On réécrit l'équation de transport sur la forme:
\[
\partial_t c + \div{c\mathbf{u}} - c \div{\mathbf{u}} = 0.
\]
et on intègre sur les cellules $[t_n, t_{n + 1}] \times T_j$:
\[
\int_{t_n}^{t_{n + 1}} \int_{T_j} \partial_t c +
\int_{t_n}^{t_{n + 1}} \int_{T_j} \div{c\mathbf{u}} -
\int_{t_n}^{t_{n + 1}} \int_{T_j} c \div{\mathbf{u}} = 0.
\]

On obtient une approximation \emph{Volumes Finis} en écrivant une version
discrète de ce bilan:
\[
\frac{c^{n + 1}_j - c^n_j}{\Delta t} +
\frac{1}{s_j} \sum_{k \in N(j)} l_{jk}\ (\mathbf{u}^n_{jk}, \mathbf{n}_{jk})\ (c^n_{jk} - c^n_j) = 0
\]
où \emph{le flux} $c^n_{jk}$ est à définir.
\end{frame}

\section{Autres schémas numériques}
\subsection{Le schéma Upwind}
\begin{frame}{Upwind}
Choix de flux classique: \emph{le flux upwind}.
\[
c^n_{jk} = \left\{
\begin{aligned}
c^n_j, & \quad \text{if } k \in N^+(j) \\
c^n_k, & \quad \text{if } k \in N^-(j).
\end{aligned}
\right.
\]

Avec ce flux, on obtient:
\[
\frac{c^{n + 1}_j - c^n_j}{\Delta t} +
\frac{1}{s_j} \sum_{k \in N^-(j)} l_{jk}\ (\mathbf{u}^n_{jk}, \mathbf{n}_{jk})\ (c^n_k - c^n_j) = 0.
\]

La mise-à-jour de $c^n_j$ à $c^{n + 1}_j$ n'utilise que les voisins en aval.
\end{frame}

% TODO: this sucks! redo
\subsection{Stabilité du schéma upwind}
\begin{frame}{Stabilité du schéma Upwind}
\[
c^{n + 1}_{j} = \left[1 + \frac{\Delta t}{s_j} \sum_{k \in N^-(j)} l_{jk}\ (\mathbf{u}^n_{jk}, \mathbf{n}_{jk}) \right] c^n_j -
                \frac{\Delta t}{s_j} \sum_{k \in N^-(j)} l_{jk}\ (\mathbf{u}^n_{jk}, \mathbf{n}_{jk})\ c^n_k.
\]

\hbox{}

La stabilité est obtenue sous la condition CFL suivante:
\begin{equation} \label{eq:cfl}
\nu^n_j = -\frac{\Delta t}{s_j} \sum_{k \in N^-(j)} l_{jk}\ (\mathbf{u}^n_{jk}, \mathbf{n}_{jk})\  \leq 1, \quad \forall j.
\end{equation}

Elle implique le \emph{principe de maximum local}:
\[
    \min_{k \in N^-(j)}(c^n_j, c^n_k) \leq c^{n + 1}_j \leq \max_{k \in N^-(j)}(c^n_j, c^n_k).
\]
\end{frame}

\begin{frame}{Stabilité du schéma Upwind}

Argument de la combinaison convexe.
\[
c^{n + 1}_{j} = \left[1 + \frac{\Delta t}{s_j} \sum_{k \in N^-(j)} l_{jk}\ (\mathbf{u}^n_{jk}, \mathbf{n}_{jk}) \right] c^n_j -
                \frac{\Delta t}{s_j} \sum_{k \in N^-(j)} l_{jk}\ (\mathbf{u}^n_{jk}, \mathbf{n}_{jk})\ c^n_k.
\]

La condition CFL $\implies c^{n + 1}_j$ est une \emph{combinaison convexe}
entre les termes $c^n_j$ et $c^n_k$ $\implies$ le \emph{principe de maximum}.

\hbox{}

La condition CFL fournit aussi une stratégie de choix de $\Delta t$.
\[
\Delta t = \theta \min_{j \in \llbracket 0, J \rrbracket}
\left[
\dfrac{-s_j}{\displaystyle \sum_{j\in N^-(j)} l_{jk}\ (\mathbf{u}^n_{jk}, \mathbf{n}_{jk})}
\right]
, \quad \theta \in [0, 1].
\]
\end{frame}

\begin{frame}{Exemple}
Le schéma upwind est stable (sous la condition CFL). Il est
aussi \emph{très diffusif}.

\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \includegraphics[width=\linewidth]{img/updown/t05.png}
        \caption{$T = 0.5$.}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \includegraphics[width=\linewidth]{img/updown/t20.png}
    \caption{$T = 20$.}
    \end{minipage}
\end{figure}

 $\Omega = [-40, 40]$, $N_x = 1000$, $\Delta t = 0.005$.
\end{frame}

\subsection{Le schéma downwind}
\begin{frame}{Downwind}
Un autre flux possible est celui donné par le schéma \emph{downwind}. Avec
ce flux, on obtient:
\[
c^{n + 1}_{j} = \left[1 + \frac{\Delta t}{s_j} \sum_{k \in N^+(j)} l_{jk}\ (\mathbf{u}^n_{jk}, \mathbf{n}_{jk}) \right] c^n_j -
                \frac{\Delta t}{s_j} \sum_{k \in N^+(j)} l_{jk}\ (\mathbf{u}^n_{jk}, \mathbf{n}_{jk})\ c^n_k.
\]

Seuls les voisins  amont contribuent à la mise-à-jour de $c^n_j$ à $c^{n + 1}_j$.

\hbox{}

Ce schéma est \emph{inconditionnellement instable}!
\end{frame}

\begin{frame}{Downwind}
\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \includegraphics[width=\linewidth]{img/updown/it3.png}
        \caption{Après $3$ pas de temps.}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \includegraphics[width=\linewidth]{img/updown/it15.png}
    \caption{Après $15$ pas de temps.}
    \end{minipage}
    \label{fig:downwind}
\end{figure}

$\Omega = [-40, 40]$, $N_x = 1000$, $\Delta t = 0.005$.
\end{frame}


\section{Vofire}
\begin{frame}{Idée}
Les lignes directrices du schéma \emph{Vofire} sont:
\begin{itemize}
    \setlength{\itemsep}{10pt}
    \item être \emph{le plus proche possible} du flux downwind
    (\textit{en un certain sens}).
    \item respecter un \emph{principe du maximum local} ($\implies$ stabilité).
\end{itemize}

\hbox{}

\hbox{}

Deux étapes de construction:
\begin{itemize}
    \setlength{\itemsep}{15pt}
    \item première étape: définition d'un \emph{flux reconstruit} $c^R_{jk}$.
    \item deuxième étape: définition du \emph{flux Vofire} $c^n_{jk}$, étape de limitation vers l'amont.
\end{itemize}
\end{frame}

\begin{frame}{Notations}
On note:
\[
a^n_{jk} = l_{jk}\ (\mathbf{u}^n_{jk}, \mathbf{n}_{jk})
\]
et:
\[
p^n_{jk} =
\left\{
\begin{aligned}
\frac{a^n_{jk}}{\sum_{k \in N^+(j)} a^n_{jk}}, & \quad \forall k \in N^+(j) \\
\frac{a^n_{jk}}{\sum_{k \in N^-(j)} a^n_{jk}}, & \quad \forall k \in N^-(j).
\end{aligned}
\right.
\]

Avec ces notations, on a:
\[
c^{n + 1}_j = \left[1 + \sum_{k \in N(j)} a^n_{jk} \right] c^n_j -
                        \sum_{k \in N(j)} a^n_{jk}\ c^n_{jk}.
\]
\end{frame}

\subsection{L'étape de reconstruction}
\begin{frame}{Première étape}
Définition du flux reconstruit:
\[
c^R_{jk} = c^n_j + \lambda^n_{jk} (c^n_k - c^n_j)
\]
pour $\forall j \in \llbracket 0, J \rrbracket, k \in N^+(j)$.

\begin{itemize}
    \setlength{\itemsep}{15pt}
    \item Une \emph{valeur reconstruite} (reconstructed value) sur chaque arête.
    \item \emph{Combinaison convexe} entre le flux upwind et le flux downwind.
    \item Comment déterminer $\lambda^n_{jk} \in [0, 1]$ de sorte à limiter la
    dissipation?
\end{itemize}

\end{frame}

\begin{frame}{Le calcul de $\lambda^n_{jk}$}
\begin{itemize}
    \setlength{\itemsep}{13pt}
    \item On ne peut pas prendre $\lambda^n_{jk} = 1$, $\forall j$ et $k \in N(j)$.
    \item $c^R_{jk}$ doit être le plus proche possible de $c^n_k$, pour $k \in N^+(j)$,
    \textit{en un certain sens}.
    \item Le sens est donné par le \emph{problème de minimisation} suivante. Soit:
    \[
    J_j = \sum_{k \in N^+(j)} p^n_{jk} |c^R_{jk} - c^n_k| =
    \sum_{k \in N^+(j)} p^n_{jk}\ |c^n_k - c^n_j| (1 - \lambda^n_{jk}).
    \]

    On cherche $\lambda^n_{jk} \in [0, 1]$, telle que $J_j$ est minimum sous la contrainte:
    \[
    \sum_{k \in N^+(j)} p^n_{jk}\ c^R_{jk} = c^n_j \equivto
    \sum_{k \in N^+(j)} p^n_{jk}\ (c^n_k - c^n_j)\ \lambda^n_{jk} = 0.
    \]
\end{itemize}
\end{frame}

\begin{frame}{Le calcul de $\lambda^n_{jk}$}
La valeur minimale de $J_j$ \emph{sous contrainte} est:
\[
\min_{\lambda^n_{jk} \in [0, 1]} J_j = |A_j - B_j|
\]
où:
\[
A_j = \sum_{k \in N^+(j)} \alpha^+_{jk}
\qquad \text { et } \qquad
B_j = -\sum_{k \in N^+(j)} \alpha^-_{jk}
\]
et:
\[
\begin{cases}
\alpha^+_{jk} = \max(p^n_{jk}\ (c^n_k - c^n_j), 0) \\
\alpha^-_{jk} = \min(p^n_{jk}\ (c^n_k - c^n_j), 0)
\end{cases}
, \forall k \in N^+(j).
\]
\end{frame}

\begin{frame}{Le calcul de $\lambda^n_{jk}$}
Pour atteindre le minimum on peut choisir des valeurs particulières de $\lambda^n_{jk}$
en fonction de $A_j$ et $B_j$.
\begin{itemize}
    \item Si $A_j = 0$ \textbf{ou} $B_j = 0$:
    \[
    \lambda^n_{jk} = 0, \forall k \in N^+(j).
    \]
    \item Si: $A_j > B_j$:
    \[
    \left\{
    \begin{aligned}
    \lambda^n_{jk} = 1, & \quad \text{pour } k \in N^+(j) \text{ telle que } \alpha_{jk} < 0 \\
    \lambda^n_{jk} = \frac{B_j}{A_j}, & \quad \text{pour } k \in N^+(j) \text{ telle que } \alpha_{jk} > 0. \\
    \end{aligned}
    \right.
    \]
    \item Si $B_j > A_j$:
    \[
    \left\{
    \begin{aligned}
    \lambda^n_{jk} = 1, & \quad \text{pour } k \in N^+(j) \text{ telle que } \alpha_{jk} > 0 \\
    \lambda^n_{jk} = \frac{A_j}{B_j}, & \quad \text{pour } k \in N^+(j) \text{ telle que } \alpha_{jk} < 0. \\
    \end{aligned}
    \right.
    \]
\end{itemize}
\end{frame}


\subsection{Le flux final}
\begin{frame}{Deuxième étape}
Le flux défini par $c^R_{jk}$ limite la diffusion numérique essentiellement dans
la \emph{direction transverse} à l'écoulement.

\hbox{}

On définit \emph{un nouveau flux} pour améliorer le résultat dans la direction de l'écoulement.
\[
c^n_{jk} = c^R_{jk} + \left[\sum_{r \in N^-(j)} p^n_{jr} \mu_{jkr}\right] (c^n_k - c^R_{jk}).
\]

Si on injecte ce flux dans l'équation de départ, on obtient:
\[
c^{n + 1}_j = \sum_{\substack{k \in N^+(j)\\ r \in N^-(j)}} p^n_{jk} p^n_{jr}
\left[(1 - \nu^n_j) c^n_j + \nu^n_j c^n_{jr} + L_j \nu^n_j \mu_{jkr} (c^n_k - c^R_{jk})\right].
\]
\end{frame}

\begin{frame}{Le calcul de $\mu_{jkr}$}
Afin de satisfaire au principe de maximum (local), on veut trouver
$\mu_{jkr} \in [0, 1]$ telle que:
\[
m_{jr} \leq (1 - \nu^n_j) c^n_j + \nu^n_j c^n_{jr} + L_j \nu^n_j \mu_{jkr} (c^n_k - c^R_{jk}) \leq M_{jr}
\]
où:
\[
m_{jr} = \min (c^n_j, c^R_{jr}) \quad \text{et}\quad M_{jr} = \max (c^n_j, c^R_{jr}).
\]

\hbox{}

La valeur maximale de $\mu_{jkr}$ qui satisfait cette inégalité est:
\[
\mu_{jkr} = \min\left(\frac{1 - \nu^n_j}{\nu^n_j}
            \max\left(\frac{m_{jr} - c^n_j}{L_j\ (c^n_k - c^R_{jk})},
                      \frac{M_{jr} - c^n_j}{L_j\ (c^n_k - c^R_{jk})}\right),
            1\right)
\]
\end{frame}

\section{Résultats numériques}
\begin{frame}{Test 1: Pacman}
\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{./img/test01/pacman01.png}
    \caption{$\mathbf{x} \mapsto c(t, \mathbf{x})$ à $t = 0$.}
    \label{fig:pacman01}
\end{figure}
\vspace{-15pt}
\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{./img/test01/pacman03.png}
    \caption{$\mathbf{x} \mapsto c(t, \mathbf{x})$ à $t = 12.312$.}
    \label{fig:pacman03}
\end{figure}

$\Omega = [-1.5, 10.5] \times [-1.5, 1.5]$, maillage rectangulaire,
$600 \times 130$ cellules, $\mathbf{u} = (1, 0)$, $\theta = 0.5$, conditions aux
limites \emph{périodiques}.
\end{frame}

\begin{frame}{Test 2: Fonction Continue}
\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{./img/test04/sombrero01.png}
    \caption{$\mathbf{x} \mapsto c(t, \mathbf{x})$ à $t = 0$.}
    \label{fig:sombrero01}
\end{figure}
\vspace{-15pt}
\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.7\linewidth]{./img/test04/sombrero02.png}
    \caption{$\mathbf{x} \mapsto c(t, \mathbf{x})$ à $t = 0.495$.}
    \label{fig:sombrero02}
\end{figure}

$\Omega = [-3.5, 7.5] \times [-2.5, 2.5]$, maillage rectangulaire,
$600 \times 130$ cellules, $\mathbf{u} = (1, 0)$, $\theta = 0.5$, conditions
aux limites \emph{périodiques}.
\end{frame}

\begin{frame}{Test 2: Fonction Continue}
\begin{figure}[!h]
    \centering
    \begin{minipage}{.46\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test04/sombreroprofile01}
        \caption{Profil à $y = 0$ et $t = 0$.}
        \label{fig:sombreroprofile01}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.46\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test04/sombreroprofile02}
        \caption{Profil à $y = 0$ et $t = 12.3$.}
        \label{fig:sombreroprofile02}
    \end{minipage}
\end{figure}
\end{frame}


\begin{frame}{Test 3: Vitesse dépendante du temps}
\begin{figure}[!h]
    \centering
    \begin{minipage}{.47\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test05/pulse01}
        \caption{$\mathbf{x} \mapsto c(t, \mathbf{x})$ à $t = 0$.}
        \label{fig:pulse01}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.47\linewidth}
        \centering
        \includegraphics[width=0.75\linewidth]{img/test05/pulse02}
        \caption{$\mathbf{x} \mapsto c(t, \mathbf{x})$ à $t = 20.0$.}
        \label{fig:pulse02}
    \end{minipage}
\end{figure}

$\Omega = [-5, 5] \times [-5, 5]$, maillage rectangulaire,
$220 \times 220$ cellules, $\mathbf{u}(t, \mathbf{x}) = (\sign{(\cos(Ft))}\ x,
\sign{(\cos(Ft))}\ y)$, $\theta = 0.5$, $T = 20$, conditions aux limites
\emph{périodiques}.
\end{frame}

\begin{frame}{Influence de chaque étape}
\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \includegraphics[width=\linewidth]{img/test07/cflinitial}
        \caption{Condition initiale.}
        \label{fig:diskinitial}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \includegraphics[width=\linewidth]{img/test07/cflmesh}
        \caption{Le maillage.}
        \label{fig:diskmesh}
    \end{minipage}
\end{figure}

$\Omega = [-7, 7] \times [-7, 7]$, maillage rectangulaire,
$150 \times 150$ cellules, $\mathbf{u} = (\pm 2 \pi y, \pm 2 \pi x)$,
$\theta = 0.1$, conditions aux limites de \emph{Neumann}.
\end{frame}

\begin{frame}{Influence de chaque étape}
\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \includegraphics[width=\linewidth]{img/test06/comparison01}
        \caption{Schéma Upwind.}
        \label{fig:comparison01}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \includegraphics[width=\linewidth]{img/test06/comparison04}
        \caption{Schéma Vofire.}
        \label{fig:comparison04}
    \end{minipage}
\end{figure}

$\Omega = [-7, 7] \times [-7, 7]$, maillage rectangulaire,
$150 \times 150$ cellules, $\mathbf{u} = (\pm 2 \pi y, \pm 2 \pi x)$,
$\theta = 0.1$, conditions aux limites de \emph{Neumann}.
\end{frame}

\begin{frame}{Influence de chaque étape}
\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \includegraphics[width=\linewidth]{img/test06/comparison02}
        \caption{$\mu = 0$.}
        \label{fig:comparison02}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \includegraphics[width=\linewidth]{img/test06/comparison03}
        \caption{$\lambda = 0$.}
        \label{fig:comparison03}
    \end{minipage}
\end{figure}

$\Omega = [-7, 7] \times [-7, 7]$, maillage rectangulaire,
$150 \times 150$ cellules, $\mathbf{u} = (\pm 2 \pi y, \pm 2 \pi x)$,
$\theta = 0.1$, conditions aux limites de \emph{Neumann}.
\end{frame}

\begin{frame}{Influence de la condition CFL}
\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.9\linewidth]{img/test07/cfl04}
        \caption{Résultat pour $\theta = 1.0$.}
        \label{fig:cfl04}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.9\linewidth]{img/test07/cfl03}
        \caption{Résultat pour $\theta = 0.7$.}
        \label{fig:cfl03}
    \end{minipage}
\end{figure}

$\Omega = [-7, 7] \times [-7, 7]$, maillage triangulaire,
$30873$ cellules, $\mathbf{u} = (\pm 2 \pi y, \pm 2 \pi x)$, $T = 1.312$,
conditions aux limites de \emph{Neumann}.
\end{frame}

\begin{frame}{Influence de la condition CFL}
\begin{figure}[!h]
    \centering
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.9\linewidth]{img/test07/cfl02}
        \caption{Résultat pour $\theta = 0.4$.}
        \label{fig:cfl02}
    \end{minipage}
    \hspace{10px}
    \begin{minipage}{.45\linewidth}
        \centering
        \includegraphics[width=0.9\linewidth]{img/test07/cfl01}
        \caption{Résultat pour $\theta = 0.1$.}
        \label{fig:cfl01}
    \end{minipage}
\end{figure}

$\Omega = [-7, 7] \times [-7, 7]$, maillage triangulaire,
$30873$ cellules, $\mathbf{u} = (\pm 2 \pi y, \pm 2 \pi x)$, $T = 1.312$,
conditions aux limites de \emph{Neumann}.
\end{frame}

\begin{frame}
\begin{center}
    {\Large Je vous remercie pour votre attention!}

    \vfill
    \includegraphics[scale=0.27]{img/questions}
    \vfill
\end{center}
\end{frame}


\end{document}



