#include <assert.h>

#include "VofireBC.h"
#include "VofireSetGet.h"
#include "VofireSolver.h"

static int Vofire_GetMeshNbOuterEdges(mesh_t *mesh) {
    // TODO: this should have a getter in YAFiVoC
    return mesh->nbOuterEdges;
}

static int Vofire_GetGhostId(problem_t *pb, edge_t *ejk) {
    mesh_t *mesh = GetPbMesh(pb);

    int nbEdges = GetMeshNbEdges(mesh);
    int nbOuterEdges = pb->solverData->nbOuterEdges;

    return GetEdgeId(ejk) - (nbEdges - nbOuterEdges);
}

static void Vofire_ReadBoundaryConditions(problem_t *pb) {
    mesh_t *mesh = GetPbMesh(pb);
    cell_t *cell = NULL;
    edge_t *edge = NULL;

    int nbOuterEdges = Vofire_GetMeshNbOuterEdges(mesh);
    int matchingEId = -1;
    int matchingCId = -1;
    char *filename = Vofire_ConfigFilename(pb, "solver.boundaryConnectivityFile");

    FILE *fd = fopen(filename, "r");
    if (!fd) {
        ExitMessage("Could not open file \"%s\".", filename);
    }

    // skip number of edges, we already have them
    fscanf(fd, "%*d");

    // read the .con file
    for (int i = 0; i < nbOuterEdges; ++i) {
        fscanf(fd, "%*d %d %d", &matchingEId, &matchingCId);

        cell = GetMeshCell(mesh, matchingCId);
        edge = GetMeshEdge(mesh, matchingEId);

        assert(cell && edge);
        assert(GetCellDataId(cell) == matchingCId);

        pb->solverData->ghosts[i].cell = cell;
        pb->solverData->ghosts[i].edge = edge;
    }

    free(filename);
    fclose(fd);
}

void Vofire_InitializeBC(problem_t *pb) {
    mesh_t *mesh = GetPbMesh(pb);
    int nbEdges = Vofire_GetMeshNbOuterEdges(mesh);

    pb->solverData->ghosts = SMalloc(nbEdges * sizeof(ghostData_t));
    pb->solverData->nbOuterEdges = nbEdges;

    Vofire_ReadBoundaryConditions(pb);
}

void Vofire_FinalizeBC(problem_t *pb) {
    free(pb->solverData->ghosts);
}

cell_t *Vofire_GetMatchingCell(problem_t *pb, edge_t *ejk) {
    int i = Vofire_GetGhostId(pb, ejk);

    return pb->solverData->ghosts[i].cell;
}

edge_t *Vofire_GetMatchingEdge(problem_t *pb, edge_t *ejk) {
    int i = Vofire_GetGhostId(pb, ejk);

    return pb->solverData->ghosts[i].edge;
}
