#ifndef VOFIRESOLVER_H_
#define VOFIRESOLVER_H_

#include <YAFiVoC.h>
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

/**
 * @brief Simple enum to denote the two components of the velocity vector.
 */
enum {
    eUx = 0,
    eUy = 1,
};

typedef struct {
    cell_t *cell;
    edge_t *edge;
} ghostData_t;

/**
 * @brief Definition of the solver data.
 */
struct solverData_t {
    varArray_t *reconstructedValue; /**< Reconstructed value on an edge. */
    varArray_t *flux;               /**< Flux on an edge.                */
    varArray_t *u;                  /**< Velocity on an edge.            */
    varArray_t *coefficients;       /**< Edge coefficient l(u, n).       */

    int nbOuterEdges;
    ghostData_t *ghosts;

    lua_State *L;    /**< The lua state.                  */
    int velocityRef; /**< Reference to the Lua function.  */
    real_t cfl;      /**< CFL condition.                  */
    int iteration;   /**< Current iteration.              */

    char *boundaryType; /**< The type of the boundary cond.  */
    int staticVelocity; /**< Velocity does not depend on time. */
    int reverse;        /**< Reverse velocity at half time.  */
    int muIsZero;       /**< See flux construction. Forces mujkr to 0.      */
    int lambdaIsZero;   /**< See flux construction. Forces lambdajk to 0.   */
};

/**
 * @brief Compute the CFL condition for our problem.
 *
 * @param[in,out] pb The problem.
 */
void MyComputeSolverCFL(problem_t *pb);

/**
 * @brief Solve one time step for our problem.
 *
 * @param[in,out] pb The problem.
 */
void MySolver(problem_t *pb);

/**
 * @brief Initialize the problem struct.
 *
 * @param[in,out] pb The problem.
 */
void MyInitializeSolver(problem_t *pb);

/**
 * @brief Print some information about the current state of the problem
 *
 * @param[in] stream File stream to write to.
 * @param[in] pb The problem.
 */
void MyPrintSolverInfo(FILE *stream, problem_t *pb);

/**
 * @brief Get the next time step for our problem.
 *
 * @param[in,out] pb The problem.
 */
real_t MyGetSolverTimeStep(problem_t *pb);

/**
 * @brief Free the problem struct.
 *
 * @param[in,out] pb The problem.
 */
void MyFinalizeSolver(problem_t *pb);

#endif /* VOFIRESOLVER_H_ */
