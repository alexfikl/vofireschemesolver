#include <YAFiVoC.h>
#include <assert.h>

#include "VofireBC.h"
#include "VofireCompute.h"
#include "VofireSetGet.h"
#include "VofireSolver.h"

/* code from Qt */
#define qFuzzyCompare(a, b) (fabs((a) - (b)) <= 0.000000000001 * MIN(fabs(a), fabs(b)))
#define EPSILON 1e-10

/* TODO: Add latex stuff where appropriate. */

/*
 * Notation used in the code:
 *  * Tj is a cell
 *      * j is used as an index for Tj
 *      * cj is the average value in Tj
 *      * uj is the speed in a cell Tj
 *      * sj is the area of Tj
 *
 *  * Tk / Tr is a neighbour of Tj
 *      * k is the index used for Tk when Tk is a downwind neighbour
 *      * r is the index used for Tr when Tr is an upwind neighbour
 *      * when considering both, use k
 *      * other variables are denoted as in the case of Tj using the correct
 *      index (sr, cr, etc.)
 *
 *  * ejk is an edge between Tj and a neighbour cell Tk
 *      * i is used as a general index when looping over edges.
 *      * jk is used when looping over the edges of Tj and considering only
 *      the downwind neighbours Tk
 *      * jr is used when looping over the edges of Tj and considering only
 *      the upwind neighbours Tr
 *      * ljk is the length of the edge
 *      * njk is the normal of the edge
 *      * ujk is the speed on the edge
 *      * cRjk and cRjr are the reconstructed values on the edge
 *      * cjk is the flux on the edge
 *
 * Other variables don't need to follow any specific naming scheme.
 */

/**
 * @brief Get the value in each cell neighbouring @c Tj.
 *
 * @param[in] pb The problem @c struct.
 * @param[in] Tj The current cell.
 * @param[in,out] c An array that will contain all the values. The size of the
 * array is given by the number of edges of @c Tj.
 */
static void Vofire_GetNeighbourCellValues(problem_t *pb, cell_t *Tj, real_t *c) {
    edge_t *ejk = NULL;
    int jk = -1;

    cell_t *Tk = NULL;

    for (EachCellEdgeId(Tj, jk)) {
        ejk = GetCellEdge(Tj, jk);

        Tk = Vofire_GetOppositeCell(pb, ejk, Tj);
        c[jk] = Vofire_GetCellValue(pb, Tk);
    }
}

/**
 * @brief Compute the coefficients p_{jk} for each edge.
 *
 * The coefficients are computed as:
 *      p_{jk} = a_{jk} / (\sum_{k \in N^-} a_{jk})     for k \in N^-
 *      p_{jk} = a_{jk} / (\sum_{k \in N^+} a_{jk})     for k \in N^+
 * where:
 *      a_{jk} = l_{jk} * (u_{jk}, n_{jk})
 * and n_{jk} is the exterior normal on the edge e_{jk}.
 *
 * @param[in] pb The problem @c struct.
 * @param[in] Tj The current cell.
 * @param[in,out] a The vector containing all the a_{jk}.
 * @param[in,out] p The vector containing all the p_{jk}.
 */
static void Vofire_ComputePjk(problem_t *pb, cell_t *Tj, real_t *a, real_t *p) {
    int jk = -1; /* edge index inside a cell */
    int i = -1;  /* global edge index */

    int theta = 0;
    real_t sumup = 0.0;
    real_t sumdown = 0.0;

    /* compute the sum of ajk on downwind and upwind edges */
    for (EachCellEdgeId(Tj, jk)) {
        i = Vofire_GetGlobalEdgeId(Tj, jk);
        theta = GetCellNormalOrientationFromEdgeId(Tj, jk);
        a[jk] = theta * Vofire_GetEdgeCoefficient(pb->solverData, i);

        /* sum all the ajk for jk in N^+(j) (downwind neighbours) */
        sumdown += (a[jk] > 0 ? a[jk] : 0);

        /* sum all the ajk for jk in N^-(j) (upwind neighbours) */
        sumup += (a[jk] < 0 ? a[jk] : 0);
    }
    assert(sumdown > 0 - EPSILON);
    assert(sumup < 0 + EPSILON);

    /* compute pjk */
    for (EachCellEdgeId(Tj, jk)) {
        if (qFuzzyCompare(sumdown + 1.0, 1.0) || qFuzzyCompare(sumup + 1.0, 1.0)) {
            p[jk] = 0.0;
        } else {
            p[jk] = (a[jk] > 0 ? a[jk] / sumdown : a[jk] / sumup);
        }

        assert(p[jk] > 0 - EPSILON);
        assert(p[jk] < 1 + EPSILON);
    }
}

/**
 * @brief Compute the constants @c Aj and @c Bj on the cell @c Tj.
 *
 * They are compute as follows:
 *      A_j = \sum_{k \in N^+} \alpha_{jk}     for \alpha_{jk} > 0
 *      B_j = \sum_{k \in N^+} \alpha_{jk}     for \alpha_{jk} < 0
 * where:
 *      \alpha_k = p_{jk} (c_k - c_j)
 *
 * @param[in] pb The problem @c struct.
 * @param[in] Tj The current cell.
 * @param[in] c An array containing the values of the neighbour cells.
 * @param[in] a An array containing the coefficients a_{jk}.
 * @param[in] p An array containing the coefficients p_{jk}.
 * @param[in,out] alpha An array containing the value of each alpha_{jk}.
 * @param[in,out] Aj The constant @b Aj on the cell @c Tj.
 * @param[in,out] Bj The constant @b Bj on the cell @c Tj.
 *
 * @sa Vofire_ComputePjk, Vofire_GetNeighbourCellValues.
 */
static void Vofire_ComputeAB(
    problem_t *pb,
    cell_t *Tj,
    real_t *c,
    real_t *a,
    real_t *p,
    real_t *alpha,
    real_t *Aj,
    real_t *Bj
) {
    real_t cj = Vofire_GetCellValue(pb, Tj);

    int jk = -1;

    *Aj = 0.0;
    *Bj = 0.0;

    for (EachCellEdgeId(Tj, jk)) {
        if (a[jk] < 0) {
            alpha[jk] = 0.0;
            continue;
        }

        alpha[jk] = p[jk] * (c[jk] - cj);
        if (alpha[jk] > 0) {
            *Aj += alpha[jk];
        } else {
            *Bj += -alpha[jk];
        }
    }
    assert(*Aj > 0 - EPSILON);
    assert(*Bj > 0 - EPSILON);
}

/**
 * @brief Compute the \lambda coefficient.
 *
 * This coefficient is used to compute the reconstructed value on each
 * edge.
 *
 * The possible values for \lambda_{jk}, for all k \in N^+, are:
 *  - if A = 0 or B = 0:
 *      \lambda_{jk} = 0
 *  - if A < B:
 *      \lambda_{jk} = 1        if \alpha_{jk} > 0
 *      \lambda_{jk} = A / B    otherwise
 *  - if A > B:
 *      \lambda_{jk} = 1        if \alpha_{jk} < 0
 *      \lambda_{jk} = B / A    otherwise
 *
 * @param[in] Tj The current cell.
 * @param[in] Aj The value of the @b Aj constant.
 * @param[in] Bj The value of the @b Bj constant.
 * @param[in] alpha An array containing the coefficients \alpha_{jk}.
 * @param[in,out] lambda An array containing the value of \lambda on each
 * edge.
 *
 * @sa Vofire_ComputeAB.
 */
static void
Vofire_ComputeLambda(cell_t *Tj, real_t Aj, real_t Bj, real_t *alpha, real_t *lambda) {
    int jk = -1;

    if (Aj < EPSILON || Bj < EPSILON) { // Aj or Bj are close to 0
        for (EachCellEdgeId(Tj, jk)) {
            lambda[jk] = 0.0;
        }
    } else if (Aj < Bj) {
        for (EachCellEdgeId(Tj, jk)) {
            lambda[jk] = (alpha[jk] > 0 ? 1.0 : Aj / Bj);

            assert(lambda[jk] > 0 - EPSILON);
            assert(lambda[jk] < 1 + EPSILON);
        }
    } else { // Aj > Bj
        for (EachCellEdgeId(Tj, jk)) {
            lambda[jk] = (alpha[jk] > 0 ? Bj / Aj : 1.0);

            assert(lambda[jk] > 0 - EPSILON);
            assert(lambda[jk] < 1 + EPSILON);
        }
    }
}

static void Vofire_ComputeMu(
    problem_t *pb, cell_t *Tj, real_t *c, real_t *a, real_t *p, real_t *delta
) {
    real_t dt = GetPbTimeStep(pb);
    real_t sj = GetCellArea(Tj);
    real_t cj = Vofire_GetCellValue(pb, Tj);

    int jk = -1;
    int jr = -1;
    int jkGlobal = -1;
    int jrGlobal = -1;

    real_t cRjk = 0.0;
    real_t cRjr = 0.0;
    real_t mjr = 0.0;
    real_t Mjr = 0.0;
    real_t mujkr = 0.0;

    /* compute the CFL condition */
    real_t nuj = 0.0;
    for (EachCellEdgeId(Tj, jk)) {
        nuj += (a[jk] < 0 ? a[jk] : 0.0);
    }
    nuj = -dt / sj * nuj;
    assert(nuj > 0.0 - EPSILON);
    assert(nuj < 1.0 + EPSILON);

    /* compute Lj */
    real_t Lj = 0.0;
    real_t sumup = 0.0;
    real_t sumdown = 0.0;
    for (EachCellEdgeId(Tj, jk)) {
        sumup += (a[jk] < 0 ? a[jk] : 0.0);
        sumdown += (a[jk] > 0 ? a[jk] : 0.0);
    }
    if (qFuzzyCompare(sumup + 1.0, 1.0)) {
        Lj = -1.0;
    } else {
        Lj = sumdown / sumup;
    }
    assert(Lj < 0.0 + EPSILON);

    /* compute each mu for upwind neighbours */
    for (EachCellEdgeId(Tj, jk)) {
        if (a[jk] < 0) { // ignore upwind neighbours
            continue;
        }

        delta[jk] = 0.0;
        jkGlobal = Vofire_GetGlobalEdgeId(Tj, jk);
        cRjk = Vofire_GetReconstructedEdgeValue(pb->solverData, jkGlobal);

        for (EachCellEdgeId(Tj, jr)) {
            if (a[jr] > 0) { // ignore downwind neighbours in the sum
                continue;
            }

            jrGlobal = Vofire_GetGlobalEdgeId(Tj, jr);
            cRjr = Vofire_GetReconstructedEdgeValue(pb->solverData, jrGlobal);

            mjr = MIN(cj, cRjr);
            Mjr = MAX(cj, cRjr);

            mjr = (1 - nuj) * (mjr - cj) / (Lj * nuj); // >= 0
            Mjr = (1 - nuj) * (Mjr - cj) / (Lj * nuj); // <= 0

            mujkr = c[jk] - cRjk;
            if (mujkr < Mjr) {
                mujkr = Mjr;
            } else if (mujkr > mjr) {
                mujkr = mjr;
            }

            delta[jk] += p[jr] * mujkr;
        }
    }
}

/**
 * @brief Compute the reconstructed value on each downwind edge of @c Tj.
 *
 * The reconstructed values are only compute on the edges of @c Tj that are
 * shared with a downwind neighbour. By going through all the cells, this
 * will eventually compute the value on almost all possible edges.
 *
 * The edges that are left out are the boundary edges for which the inner
 * cell is a downwind cell. These edges are treated separately.
 *
 * @param[in] pb The problem @c struct.
 * @param[in] Tj The current cell.
 *
 * @sa ComputeAB, ComputeLambda.
 */
static void Vofire_ComputeReconstructedEdgeValueByCell(problem_t *pb, cell_t *Tj) {
    int lambdaIsZero = pb->solverData->lambdaIsZero;

    real_t cj = Vofire_GetCellValue(pb, Tj);
    real_t nbEdges = GetCellNbEdges(Tj);

    edge_t *ejk = NULL;
    edge_t *ekj = NULL;

    int jk = -1;
    int i = -1;

    real_t cRjk = 0.0;
    real_t Aj = 0.0;
    real_t Bj = 0.0;

    real_t *c = NULL;
    real_t *a = NULL;
    real_t *p = NULL;
    real_t *alpha = NULL;
    real_t *lambda = NULL;

    c = SMalloc(nbEdges * sizeof(real_t));
    a = SMalloc(nbEdges * sizeof(real_t));
    p = SMalloc(nbEdges * sizeof(real_t));
    alpha = SMalloc(nbEdges * sizeof(real_t));
    lambda = SMalloc(nbEdges * sizeof(real_t));

    Vofire_GetNeighbourCellValues(pb, Tj, c);
    Vofire_ComputePjk(pb, Tj, a, p);
    Vofire_ComputeAB(pb, Tj, c, a, p, alpha, &Aj, &Bj);
    Vofire_ComputeLambda(Tj, Aj, Bj, alpha, lambda);

    /* compute the reconstructed values */
    for (EachCellEdgeId(Tj, jk)) {
        ejk = GetCellEdge(Tj, jk);
        i = GetEdgeId(ejk);

        /* skip if upwind cell. */
        if (a[jk] < 0) {
            continue;
        }

        /* compute flux */
        if (lambdaIsZero) { // lambda is forced to zero
            cRjk = cj;
        } else { // use computed lambda
            cRjk = cj + lambda[jk] * (c[jk] - cj);

            //             assert(cRjk <= MAX(cj, c[jk]) + EPSILON);
            //             assert(cRjk >= MIN(cj, c[jk]) - EPSILON);
        }

        Vofire_SetReconstructedEdgeValue(pb->solverData, i, cRjk);
        if (IsABoundaryEdge(ejk)) {
            ekj = Vofire_GetMatchingEdge(pb, ejk);
            i = GetEdgeId(ekj);
            Vofire_SetFlux(pb->solverData, i, cj);
        }
    }

    free(c);
    free(a);
    free(p);
    free(alpha);
    free(lambda);
}

/**
 * @brief Compute the reconstructed value on each edge.
 *
 * @param[in] pb The problem @c struct.
 *
 * @sa ComputeReconstructedEdgeValueByCell.
 */
static void Vofire_ComputeReconstructedEdgeValue(problem_t *pb) {
    mesh_t *mesh = GetPbMesh(pb);
    cell_t *Tj = NULL;

    for (EachMeshCell(mesh, Tj)) {
        Vofire_ComputeReconstructedEdgeValueByCell(pb, Tj);
    }
}

static void Vofire_ComputeCellDownwindFlux(problem_t *pb, cell_t *Tj) {
    int muIsZero = pb->solverData->muIsZero;

    int numberOfEdges = GetCellNbEdges(Tj);
    edge_t *ejk = NULL;
    edge_t *ekj = NULL;

    int jk = -1;
    int i = -1;

    real_t cRjk = 0.0;
    real_t cjk = 0.0;

    real_t *c = NULL;
    real_t *a = NULL;
    real_t *p = NULL;
    real_t *delta = NULL;

    c = SMalloc(numberOfEdges * sizeof(real_t));
    a = SMalloc(numberOfEdges * sizeof(real_t));
    p = SMalloc(numberOfEdges * sizeof(real_t));
    delta = SMalloc(numberOfEdges * sizeof(real_t));

    Vofire_GetNeighbourCellValues(pb, Tj, c);
    Vofire_ComputePjk(pb, Tj, a, p);
    Vofire_ComputeMu(pb, Tj, c, a, p, delta);

    /* and finally the fluxes */
    for (EachCellEdgeId(Tj, jk)) {
        ejk = GetCellEdge(Tj, jk);
        i = GetEdgeId(ejk);

        /* skip upwind edges */
        if (a[jk] < 0) {
            continue;
        }

        /* compute the flux */
        if (muIsZero) { // force mu to zero
            cjk = Vofire_GetReconstructedEdgeValue(pb->solverData, i);
        } else { // use the computed value of mu
            cRjk = Vofire_GetReconstructedEdgeValue(pb->solverData, i);
            cjk = cRjk + delta[jk];

            //             assert(cjk <= MAX(cRjk, c[jk]) + EPSILON);
            //             assert(cjk >= MIN(cRjk, c[jk]) - EPSILON);
        }

        Vofire_SetFlux(pb->solverData, i, cjk);
        if (IsABoundaryEdge(ejk)) {
            ekj = Vofire_GetMatchingEdge(pb, ejk);
            i = GetEdgeId(ekj);
            Vofire_SetFlux(pb->solverData, i, cjk);
        }
    }

    free(c);
    free(a);
    free(p);
    free(delta);
}

/**
 * @brief Compute the velocity on an interior edge.
 *
 * @param[in] pb The problem @c struct.
 * @param[in] edge The edge.
 *
 * @sa Vofire_ComputeEdgeVelocity.
 */
static void Vofire_ComputeSingleEdgeVelocity(problem_t *pb, edge_t *edge) {
    real_t t = GetPbTime(pb);
    cell_t *Tj = NULL;
    cell_t *Tk = NULL;

    if (IsABoundaryEdge(edge)) {
        Tj = GetEdgeInnerCell(edge);
        Tk = Vofire_GetOppositeCell(pb, edge, Tj);
    } else {
        Tj = GetEdgeCellW(edge);
        Tk = GetEdgeCellE(edge);
    }

    vect_t uj = Vofire_GetCellVelocity(pb, Tj, t);
    vect_t uk = Vofire_GetCellVelocity(pb, Tk, t);

    /* compute the velocity on the edge: mean between the two cells */
    int i = GetEdgeId(edge);
    vect_t ujk = {.x = (uj.x + uk.x) / 2.0, .y = (uj.y + uk.y) / 2.0};

    Vofire_SetEdgeVelocity(pb->solverData, i, ujk);
}

void Vofire_ComputeEdgeVelocity(problem_t *pb) {
    mesh_t *mesh = GetPbMesh(pb);
    edge_t *e = NULL;

    for (EachMeshEdge(mesh, e)) {
        Vofire_ComputeSingleEdgeVelocity(pb, e);
    }
}

void Vofire_ComputeEdgeCoefficient(problem_t *pb) {
    mesh_t *mesh = GetPbMesh(pb);
    edge_t *e = NULL;

    int i = -1;

    vect_t ujk;
    vect_t njk;
    real_t ljk = 0.0;
    real_t ajk = 0.0;

    for (EachMeshEdge(mesh, e)) {
        i = GetEdgeId(e);
        ujk = Vofire_GetEdgeVelocity(pb->solverData, i);

        /* compute the value of the coefficient ajk for this edge */
        njk = GetEdgeNormal(e);
        ljk = GetEdgeLength(e);
        ajk = ljk * (ujk.x * njk.x + ujk.y * njk.y);

        Vofire_SetEdgeCoefficient(pb->solverData, i, ajk);
    }
}

void Vofire_ComputeTimeStep(problem_t *pb) {
    real_t CFL = pb->solverData->cfl;
    mesh_t *mesh = GetPbMesh(pb);

    cell_t *Tj = NULL;
    real_t sj = 0.0;
    int theta = 0;

    int i = -1;
    int jk = -1;
    real_t ajk = 0.0;

    real_t sum = 0.0;
    real_t dt = 0.0;
    real_t dtmin = 2.0;

    for (EachMeshCell(mesh, Tj)) {
        sj = GetCellArea(Tj);
        sum = 0.0;

        for (EachCellEdgeId(Tj, jk)) {
            i = Vofire_GetGlobalEdgeId(Tj, jk);
            theta = GetCellNormalOrientationFromEdgeId(Tj, jk);
            ajk = theta * Vofire_GetEdgeCoefficient(pb->solverData, i);

            sum += (ajk < 0 ? ajk : 0.0);
        }

        if (qFuzzyCompare(sum + 1.0, 1.0)) {
            continue;
        }

        dt = -sj / sum * CFL;
        dtmin = MIN(dt, dtmin);
    }
    assert(dt > 0.0 - EPSILON);
    assert(dt < 1.0 + EPSILON);

    SetPbTimeStep(pb, dtmin);
}

void Vofire_ComputeFlux(problem_t *pb) {
    mesh_t *mesh = GetPbMesh(pb);
    cell_t *Tj = NULL;

    Vofire_ComputeReconstructedEdgeValue(pb);

    for (EachMeshCell(mesh, Tj)) {
        Vofire_ComputeCellDownwindFlux(pb, Tj);
    }
}

void Vofire_UpdateCellValues(problem_t *pb) {
    mesh_t *mesh = GetPbMesh(pb);
    double dt = GetPbTimeStep(pb);

    cell_t *Tj = NULL;
    real_t cj = 0.0;
    real_t sj = 0.0;
    int theta = 0;

    int jk = -1;
    int i = -1;
    int j = -1;

    real_t ajk = 0.0;
    real_t cjk = 0.0;
    real_t *delta = NULL;

    delta = SMalloc(GetMeshNbCells(mesh) * sizeof(real_t));

    for (EachMeshCell(mesh, Tj)) {
        j = GetCellDataId(Tj);
        sj = GetCellArea(Tj);
        cj = Vofire_GetCellValue(pb, Tj);
        delta[j] = 0.0;

        for (EachCellEdgeId(Tj, jk)) {
            i = Vofire_GetGlobalEdgeId(Tj, jk);
            theta = GetCellNormalOrientationFromEdgeId(Tj, jk);
            cjk = Vofire_GetFlux(pb->solverData, i);
            ajk = theta * Vofire_GetEdgeCoefficient(pb->solverData, i);

            delta[j] += (dt / sj) * ajk * (cj - cjk);
        }
    }

    for (EachMeshCell(mesh, Tj)) {
        j = GetCellDataId(Tj);
        cj = Vofire_GetCellValue(pb, Tj);

        cj = cj + delta[j];

        Vofire_SetCellValue(pb, Tj, cj);
    }

    free(delta);
}

void Vofire_ReverseVelocity(problem_t *pb) {
    mesh_t *mesh = GetPbMesh(pb);
    varArray_t *data = GetPbData(pb);

    cell_t *T = NULL;
    int j = -1;

    real_t rhoux = 0.0;
    real_t rhouy = 0.0;

    for (EachMeshCell(mesh, T)) {
        j = GetCellDataId(T);

        rhoux = VarArrayGetRhoUx(data, j);
        rhouy = VarArrayGetRhoUy(data, j);

        VarArraySetRhoUx(data, j, -rhoux);
        VarArraySetRhoUy(data, j, -rhouy);
    }
}
