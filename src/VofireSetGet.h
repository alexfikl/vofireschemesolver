#ifndef VOFIRESETGET_H_
#define VOFIRESETGET_H_

#include <YAFiVoC.h>

#define qFuzzyCompare(a, b) (fabs((a) - (b)) <= 0.000000000001 * MIN(fabs(a), fabs(b)))

/**
 * @brief Get the velocity on an edge.
 *
 * @param[in] data The solver data array.
 * @param[in] i The edge ID.
 *
 * @return The velocity vector.
 */
vect_t Vofire_GetEdgeVelocity(solverData_t *data, int i);

/**
 * @brief Set the velocity on an edge.
 *
 * @param[in,out] data The solver data array.
 * @param[in] i The edge ID.
 * @param[in] value The new value on the ith edge.
 */
void Vofire_SetEdgeVelocity(solverData_t *data, int i, vect_t value);

/**
 * @brief Get the reconstructed edge value.
 *
 * @param[in] data The solver data array.
 * @param[in] i The edge ID.
 *
 * @return The reconstructed value on the ith edge.
 */
real_t Vofire_GetReconstructedEdgeValue(solverData_t *data, int i);

/**
 * @brief Set the reconstructed value on an edge.
 *
 * @param[in,out] data The solver data array.
 * @param[in] i The edge ID.
 * @param[in] value The new value on the ith edge.
 */
void Vofire_SetReconstructedEdgeValue(solverData_t *data, int i, real_t value);

/**
 * @brief Get the flux on an edge.
 *
 * @param[in] data The solver data array.
 * @param[in] i The edge ID.
 *
 * @return The flux on the ith edge.
 */
real_t Vofire_GetFlux(solverData_t *data, int i);

/**
 * @brief Set the flux on an edge.
 *
 * @param[in,out] data The solver data array.
 * @param[in] i The edge ID.
 * @param[in] value The new value on the ith edge.
 */
void Vofire_SetFlux(solverData_t *data, int i, real_t value);

/**
 * @brief Get the edge coefficient.
 *
 * The edge coefficient is:
 *      l_jk (u_jk, n_jk)
 * where l_jk is the length of the edge, u_jk is the velocity on the edge and
 * n_jk is the edge normal.
 *
 * @param[in] data The solver data array.
 * @param[in] i The edge ID.
 *
 * @return The coefficient on the ith edge.
 */
real_t Vofire_GetEdgeCoefficient(solverData_t *data, int i);

/**
 * @brief Set the coefficient on an edge.
 *
 * @param[in,out] data The solver data array.
 * @param[in] i The edge ID.
 * @param[in] value The new value on the ith edge.
 */
void Vofire_SetEdgeCoefficient(solverData_t *data, int i, real_t value);

/**
 * @brief Get the value in a cell.
 *
 * @param[in] pb The problem struct.
 * @param[in] Tj A cell.
 *
 * @return The value in the cell Tj.
 */
real_t Vofire_GetCellValue(problem_t *pb, cell_t *Tj);

/**
 * @brief Set the value in a cell.
 *
 * @param[in,out] pb The problem struct.
 * @param[in] Tj A cell.
 * @param[in] value The new value.
 */
void Vofire_SetCellValue(problem_t *pb, cell_t *Tj, real_t value);

/**
 * @brief Compute the velocity in a cell at a certain time t.
 *
 * @param[in] t The time.
 * @param[in] T The cell.
 *
 * @returns The velocity in the cell at time t.
 */
vect_t Vofire_GetCellVelocity(problem_t *pb, cell_t *T, real_t t);

/**
 * @brief Get the cell of the other side of an edge.
 *
 * Given an edge ejk that belongs to a cell T, this function will return the
 * other cell that contains ejk. If ejk is a boundary edge, it will return T.
 *
 * @param[in] ejk The edge.
 * @param[in] T The edge.
 *
 * @returns The opposite cell.
 */
cell_t *Vofire_GetOppositeCell(problem_t *pb, edge_t *ejk, cell_t *T);

/**
 * @brief Get the global edge ID from the ID inside a cell.
 *
 * @param[in] Tj A cell.
 * @param[in] k The ID of an edge inside the cell.
 *
 * @return The global ID of that edge.
 */
int Vofire_GetGlobalEdgeId(cell_t *Tj, int k);

/**
 * @brief Read a filename from the configuration file.
 *
 * This returns the absolute filename of the requested file.
 *
 * @param[in] pb The problem struct.
 * @param[in] key They key of the filename in the configuration file.
 *
 * @return The absolute path of the file.
 */
char *Vofire_ConfigFilename(problem_t *pb, const char *key);

vect_t Vofire_GetLuaVelocity(problem_t *pb, real_t t, real_t x, real_t y);

#endif /* VOFIRESETGET_H_ */
