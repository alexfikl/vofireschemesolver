#ifndef VOFIREBC_H_
#define VOFIREBC_H_

#include <YAFiVoC.h>

void Vofire_InitializeBC(problem_t *pb);

void Vofire_FinalizeBC(problem_t *pb);

cell_t *Vofire_GetMatchingCell(problem_t *pb, edge_t *ejk);

edge_t *Vofire_GetMatchingEdge(problem_t *pb, edge_t *ejk);

#endif /* VOFIREBC_H_ */
