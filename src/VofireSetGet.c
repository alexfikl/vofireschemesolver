#include <YAFiVoC.h>
#include <math.h>

#include "VofireBC.h"
#include "VofireSetGet.h"
#include "VofireSolver.h"

#define FREQUENCY 3

vect_t Vofire_GetEdgeVelocity(solverData_t *data, int i) {
    vect_t result = {.x = GetVal(data->u, i, eUx), .y = GetVal(data->u, i, eUy)};

    return result;
}

void Vofire_SetEdgeVelocity(solverData_t *data, int i, vect_t value) {
    SetVal(data->u, i, eUx, value.x);
    SetVal(data->u, i, eUy, value.y);
}

real_t Vofire_GetReconstructedEdgeValue(solverData_t *data, int i) {
    return GetVal(data->reconstructedValue, i, 0);
}

void Vofire_SetReconstructedEdgeValue(solverData_t *data, int i, real_t value) {
    SetVal(data->reconstructedValue, i, 0, value);
}

real_t Vofire_GetFlux(solverData_t *data, int i) {
    return GetVal(data->flux, i, 0);
}

void Vofire_SetFlux(solverData_t *data, int i, real_t value) {
    SetVal(data->flux, i, 0, value);
}

real_t Vofire_GetEdgeCoefficient(solverData_t *data, int i) {
    return GetVal(data->coefficients, i, 0);
}

void Vofire_SetEdgeCoefficient(solverData_t *data, int i, real_t value) {
    SetVal(data->coefficients, i, 0, value);
}

real_t Vofire_GetCellValue(problem_t *pb, cell_t *Tj) {
    varArray_t *data = GetPbData(pb);
    int j = GetCellDataId(Tj);

    return VarArrayGetRho(data, j);
}

void Vofire_SetCellValue(problem_t *pb, cell_t *Tj, real_t value) {
    varArray_t *data = GetPbData(pb);

    int j = GetCellDataId(Tj);
    vect_t uj = VarArrayGetVelocity(data, j);

    VarArraySetRho(data, j, value);

    /* TODO: this should be removed when we find a better way to store our c */
    VarArraySetRhoUx(data, j, value * uj.x);
    VarArraySetRhoUy(data, j, value * uj.y);
}

vect_t Vofire_GetCellVelocity(problem_t *pb, cell_t *T, real_t t) {
    varArray_t *data = GetPbData(pb);

    int nbNodes = 0;
    int i = -1;
    node_t *node = NULL;
    double x, y;

    vect_t tmp;
    vect_t velocity = {.x = 0, .y = 0};

    if (pb->solverData->staticVelocity) {
        i = GetCellDataId(T);
        velocity.x = VarArrayGetUx(data, i);
        velocity.y = VarArrayGetUy(data, i);
    } else {
        for (EachCellNodeId(T, i)) {
            node = GetCellNode(T, i);
            x = GetNodeCoord_x(node);
            y = GetNodeCoord_y(node);

            tmp = Vofire_GetLuaVelocity(pb, t, x, y);

            velocity.x += tmp.x;
            velocity.y += tmp.y;
            ++nbNodes;
        }

        velocity.x = velocity.x / nbNodes;
        velocity.y = velocity.y / nbNodes;
    }

    return velocity;
}

cell_t *Vofire_GetOppositeCell(problem_t *pb, edge_t *ejk, cell_t *T) {
    if (!IsABoundaryEdge(ejk)) {
        cell_t *Tj = GetEdgeCellW(ejk);
        cell_t *Tk = GetEdgeCellE(ejk);

        return (T == Tj ? Tk : Tj);
    } else {
        return Vofire_GetMatchingCell(pb, ejk);
    }
}

int Vofire_GetGlobalEdgeId(cell_t *Tj, int k) {
    edge_t *ejk = GetCellEdge(Tj, k);
    return GetEdgeId(ejk);
}

char *Vofire_ConfigFilename(problem_t *pb, const char *key) {
    char *relativeFilename = Config_ReadString(pb->cfg, key);
    char *dir = ForgeAbsolutePath(GetPbWorkingDirectory(pb));
    char *absPath = ForgeJointPath(dir, relativeFilename);

    free(relativeFilename);
    free(dir);

    return absPath;
}

vect_t Vofire_GetLuaVelocity(problem_t *pb, real_t t, real_t x, real_t y) {
    vect_t result;
    lua_State *L = pb->solverData->L;
    int ref = pb->solverData->velocityRef;

    lua_rawgeti(L, LUA_REGISTRYINDEX, ref);
    lua_pushnumber(L, t);
    lua_pushnumber(L, x);
    lua_pushnumber(L, y);

    if (lua_pcall(L, 3, 2, 0) != 0) {
        ExitMessage("error: function call failed.\n");
    }

    if (!lua_isnumber(L, -1) || !lua_isnumber(L, -2)) {
        ExitMessage("error: return values are not numbers.\n");
    }

    result.y = lua_tonumber(L, -1);
    result.x = lua_tonumber(L, -2);
    lua_pop(L, 2);

    return result;
}
