#include <YAFiVoC.h>

#include "VofireBC.h"
#include "VofireCompute.h"
#include "VofireSetGet.h"
#include "VofireSolver.h"

static void Vofire_IncrementIteration(problem_t *pb) {
    pb->solverData->iteration += 1;
}

static int Vofire_ShouldReverseVelocity(problem_t *pb) {
    return pb->solverData->reverse && (GetPbTime(pb) > (GetPbTMax(pb) / 2.0));
}

static void Vofire_InitializeVelocity(problem_t *pb) {
    char *filename = Vofire_ConfigFilename(pb, "solver.velocityFile");
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);

    if (luaL_dofile(L, filename)) {
        lua_close(L);
        ExitMessage("error: cannot open lua file \'%s\'\n", filename);
    }

    lua_getglobal(L, "Velocity");
    if (!lua_isfunction(L, -1)) {
        lua_close(L);
        ExitMessage("error: cannot load function \'Velocity\'\n");
    }

    pb->solverData->velocityRef = luaL_ref(L, LUA_REGISTRYINDEX);
    pb->solverData->L = L;

    free(filename);
}

static void Vofire_FinalizeVelocity(problem_t *pb) {
    lua_close(pb->solverData->L);
}

void MyComputeSolverCFL(problem_t *pb) {
    UNUSED(pb);
}

void MySolver(problem_t *pb) {
    if (Vofire_ShouldReverseVelocity(pb)) {
        pb->solverData->reverse = 0;
        Vofire_ReverseVelocity(pb);
    }

    // compute the edge velocity
    Vofire_ComputeEdgeVelocity(pb);
    Vofire_ComputeEdgeCoefficient(pb);

    // compute the time step that satisfies the CFL condition
    Vofire_ComputeTimeStep(pb);

    // compute the flux on each edge
    Vofire_ComputeFlux(pb);

    // update the values in each cell
    Vofire_UpdateCellValues(pb);

    // ta da! finished!
    IncrementTimeStep(pb);
    //     SetPbTime(pb, 2); // stop!
    Vofire_IncrementIteration(pb);
}

void MyInitializeSolver(problem_t *pb) {
    solverData_t *solverData = NULL;
    mesh_t *mesh = GetPbMesh(pb);
    int nbEdges = GetMeshNbEdges(mesh);

    /* allocate memory for our solverData in the global problem struct */
    solverData = SMalloc(sizeof(solverData_t));
    pb->solverData = solverData;

    solverData->reconstructedValue = NewVarArray(nbEdges, 1);
    solverData->flux = NewVarArray(nbEdges, 1);
    solverData->u = NewVarArray(nbEdges, 2);
    solverData->coefficients = NewVarArray(nbEdges, 1);
    solverData->iteration = 0;

    solverData->boundaryType = Config_ReadString(pb->cfg, "solver.boundaryType");
    solverData->staticVelocity = Config_ReadInt(pb->cfg, "solver.staticVelocity");
    solverData->reverse = Config_ReadInt(pb->cfg, "solver.halfReverse");
    solverData->cfl = Config_ReadReal(pb->cfg, "solver.CFL");
    solverData->muIsZero = Config_ReadInt(pb->cfg, "solver.muIsZero");
    solverData->lambdaIsZero = Config_ReadInt(pb->cfg, "solver.lambdaIsZero");

    if (!solverData->staticVelocity) {
        Vofire_InitializeVelocity(pb);
    }

    Vofire_InitializeBC(pb);
}

void MyPrintSolverInfo(FILE *stream, problem_t *pb) {
    fprintf(stream, "Solver: VOFIRE\n");
    fprintf(stream, "Boundary Conditions: %s\n", pb->solverData->boundaryType);
    fprintf(stream, "n  = %d\n", pb->solverData->iteration);
    fprintf(stream, "t  = %g\n", GetPbTime(pb));
    fprintf(stream, "dt = %g\n", GetPbTimeStep(pb));
}

real_t MyGetSolverTimeStep(problem_t *pb) {
    return GetPbTimeStep(pb);
}

void MyFinalizeSolver(problem_t *pb) {
    Vofire_FinalizeVelocity(pb);
    Vofire_FinalizeBC(pb);

    free(pb->solverData->boundaryType);

    FreeVarArray(pb->solverData->reconstructedValue);
    FreeVarArray(pb->solverData->flux);
    FreeVarArray(pb->solverData->u);
    FreeVarArray(pb->solverData->coefficients);

    free(pb->solverData);
}
