#ifndef VOFIRECOMPUTE_H_
#define VOFIRECOMPUTE_H_

#include <YAFiVoC.h>

/**
 * @brief Compute the velocity on each edge.
 *
 * The velocity on an edge is computed by using the velocities in the
 * two cells the edge belongs to.
 *
 * This will update the values in the solverData member of the pb struct.
 *
 * @param[in,out] pb The problem @c struct.
 */
void Vofire_ComputeEdgeVelocity(problem_t *pb);

/**
 * @brief Compute the coefficients on each edge.
 *
 * The coefficient of an edge ejk is:
 *      ljk (ujk, njk)
 * where ljk is the edge length, ujk is the edge velocity and njk is the
 * edge normal. It is denoted by ajk in the formulae and in the code.
 *
 * This coefficient is computed separately from the velocity because it is
 * often used in the code to see if a cell is upwind or downwind and to
 * update the new cell values.
 *
 * @param[in,out] pb The problem struct.
 */
void Vofire_ComputeEdgeCoefficient(problem_t *pb);

/**
 * @brief Compute the flux on each edge.
 *
 * The flux is computed using the Vofire method.
 *
 * @param[in,out] pb The problem @c struct.
 *
 * @see http://www.latp.univ-mrs.fr/IJFV/IMG/pdf/Despres_ijfv_2.pdf
 */
void Vofire_ComputeFlux(problem_t *pb);

/**
 * @brief Compute the time step that satisfied a CFL condition.
 *
 * The time step is compute using the formula:
 *      dt = min_j (sj / \sum_{k \in N^-(j)} a_{jk})
 *
 * This is the maximum time step that satisfies the stability requirements.
 *
 * @param[in,out] pb The problem @c struct.
 */
void Vofire_ComputeTimeStep(problem_t *pb);

/**
 * @brief Update the value in each cell.
 *
 * The value is update using the formula:
 *      c^{n + 1}_j = c^n_j - dt / s_j \sum_{k \in N(j)} a_{jk} (c^n_{jk} - c^n_j)
 * where c^n_{jk} is the flux on the edge e_{jk}.
 *
 * @param[in,out] pb The problem @c struct.
 *
 * @see http://www.latp.univ-mrs.fr/IJFV/IMG/pdf/Despres_ijfv_2.pdf
 */
void Vofire_UpdateCellValues(problem_t *pb);

/**
 * @brief Reverse the direction of the velocity.
 *
 * The new velocity is:
 *  (ux,  uy) = (-ux, -uy)
 *
 * @param[in,out] pb The problem struct.
 */
void Vofire_ReverseVelocity(problem_t *pb);

#endif /* VOFIRECOMPUTE_H_ */
