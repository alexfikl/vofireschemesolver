PYTHON := "python -X dev"
BUILDDIR := "build"

_default:
    @just --list

# {{{ format

[doc("Reformat all source code")]
format: justfmt isort ruffmt mesonfmt clangfmt

[private]
clang_format directory:
    find {{ directory }} -type f -name '*.c' -exec clang-format -i {} \;
    find {{ directory }} -type f -name '*.h' -exec clang-format -i {} \;

[doc("Run clang-format over all source files")]
clangfmt:
    @just clang_format src

[doc('Run ruff isort fixes over the source code')]
isort:
    ruff check --fix --select=I tests
    ruff check --fix --select=RUF022 tests
    @echo -e "\e[1;32mruff isort clean!\e[0m"

[doc('Run ruff format over the source code')]
ruffmt:
    ruff format tests
    @echo -e "\e[1;32mruff format clean!\e[0m"

[doc("Run meson format over all meson files")]
mesonfmt:
    meson format --inplace --recursive
    @echo -e "\e[1;32mmeson format clean!\e[0m"

[doc("Run just --fmt over the justfiles")]
justfmt:
    just --unstable --fmt
    just -f docs/justfile --unstable --fmt
    @echo -e "\e[1;32mjust --fmt clean!\e[0m"

# }}}
# {{{ lint

[doc("Run all linting checks over the source code")]
lint: typos ruff

[doc("Run typos over the source code and documentation")]
typos:
    typos --sort
    @echo -e "\e[1;32mtypos clean!\e[0m"

[doc('Run ruff checks over the source code')]
ruff:
    ruff check tests
    @echo -e "\e[1;32mruff clean!\e[0m"

# }}}
# {{{ develop

[doc("Build project in debug mode")]
debug compiler="clang": purge && compile
    CC={{ compiler }} meson setup \
        --buildtype=debug \
        {{ BUILDDIR }}

[doc("Compile the project")]
compile:
    meson compile -C {{ BUILDDIR }}

[doc("Regenerate ctags")]
ctags:
    ctags --recurse=yes \
        --tag-relative=yes \
        --exclude=.git \
        --exclude=docs

[doc("Remove various build artifacts")]
clean:
    rm -rf {{ BUILDDIR }}

[doc("Remove all generated build files")]
purge: clean
    rm -rf {{ BUILDDIR }} tags

# }}}
