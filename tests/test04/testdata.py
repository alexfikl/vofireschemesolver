from __future__ import annotations

import math
from collections import namedtuple

Options = namedtuple("Options", "xspan, yspan, shape")

MESH_OPTS = Options([-7, 7], [-7, 7], [150, 150])


def DataFunction(x, y):
    r = 1.2
    cx = 3.5
    cy = 0

    rho = ((x - cx) ** 2 + (y - cy) ** 2 < r**2) + 1
    u = -2 * math.pi * y
    v = 2 * math.pi * x
    P = 1

    return rho, u, v, P
