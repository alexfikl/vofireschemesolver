#!/usr/bin/env python

from __future__ import annotations

import datetime
from collections import namedtuple

import numpy as np

Vertex = namedtuple("Vertex", "vid x y region")
Edge = namedtuple("Edge", "eid vertices cells length normal region")
Ghost = namedtuple("Ghost", "e me mc")
Cell = namedtuple("Cell", "cid vertices edges center area")


class Region:
    West = 1
    North = 2
    East = 3
    South = 4
    Corner = 42
    Interior = 0


def NormalOrientation():
    South = 1
    East = 1
    North = -1
    West = -1

    return [South, North, West, East]


def NodeRegion(x, y, xspan, yspan):
    if (
        np.allclose([x, y], [xspan[0], yspan[0]])
        or np.allclose([x, y], [xspan[0], yspan[1]])
        or np.allclose([x, y], [xspan[1], yspan[0]])
        or np.allclose([x, y], [xspan[1], yspan[1]])
    ):
        return Region.Corner

    if np.allclose(x, xspan[0]):
        return Region.West
    if np.allclose(x, xspan[1]):
        return Region.East
    if np.allclose(y, yspan[0]):
        return Region.North
    if np.allclose(y, yspan[1]):
        return Region.South
    return Region.Interior


def EdgeLength(A, B):
    return np.sqrt((A.x - B.x) ** 2 + (A.y - B.y) ** 2)


def CellCenter(vertices):
    cx = np.mean([v.x for v in vertices])
    cy = np.mean([v.y for v in vertices])

    return cx, cy


def CellArea(vertices):
    x = np.array([v.x for v in [*vertices, vertices[0]]])
    y = np.array([v.y for v in [*vertices, vertices[0]]])

    area = np.sum(x[:-1] * y[1:] - x[1:] * y[:-1])

    return 0.5 * np.abs(area)


def GetEdgeInnerCell(e):
    return e.cells[0] if e.cells[0] else e.cells[1]


class Quadrangle:
    """
    Example of the numbering:
       x      0 (0) 1 (1) 2 (2) 3 (4) 4
    o-->      o-----o-----o-----o-----o
    |    (20) |  0  |  1  |  2  |  3  | (36)
    v y       o-----o-----o-----o-----o
         (21) |     |     |     |     | (37)
              o-----o-----o-----o-----o
         (22) |     |     |     |     | (38)
              o-----o-----o-----o-----o
         (23) | 12  | 13  | 14  | 15  | (39)
              o-----o-----o-----o-----o
              20(16)21(17)22(18)23(19) 24

    where:
    * nodes are numberd from 0 to NxM with x as the principal direction.
    * edges (in parenthesis) are numbered first horizontally and then
    vertically.
    * cells (inside the squares) are numbered by x and then by y, as the
    nodes.
    """

    def __init__(self, xspan, yspan, shape):
        self.xspan = xspan
        self.yspan = yspan
        self.nx = shape[0] - 1
        self.ny = shape[1] - 1

        self.nbNodes = (self.nx + 1) * (self.ny + 1)
        self.nbEdges = (self.nx + 1) * self.ny + self.nx * (self.ny + 1)
        self.nbCells = self.nx * self.ny

        # start of vertical edges
        self.edgeOffset = (self.ny + 1) * self.nx

        # get the date
        self.date = datetime.datetime.today().strftime("%d-%m-%Y")

    def GenerateMesh(self, *, periodic=False, sortEdges=True):
        self.periodic = periodic

        print("Generating mesh..")
        self._CreateNodes(self.xspan, self.yspan)
        self._CreateEdges()
        self._CreateCells()
        self._BoundaryConnectivity()

        if sortEdges:
            self._SortEdgesByRegion()

    def WriteMesh(self, basename):
        print("Writing mesh..")
        filename = f"{basename}.node"
        self._WriteFile(self.node, filename, self._VertexStr)

        filename = f"{basename}.edge"
        self._WriteFile(self.edge, filename, self._EdgeStr)

        filename = f"{basename}.ele"
        self._WriteFile(self.cell, filename, self._CellStr)

        filename = f"{basename}.conn"
        self._WriteFile(self.ghost, filename, self._GhostStr)

    def _WriteFile(self, array, filename, tostr):
        with open(filename, "w") as fd:
            fd.write(f"{len(array)}\n")

            for element in array:
                fd.write(f"{tostr(element)}\n")

            fd.write(f"\n# written with quadrangle.py {self.date}")

    def _VertexStr(self, v):
        return f"{v.vid} {v.x:g} {v.y:g} {v.region}"

    def _EdgeStr(self, e):
        cellId = lambda c: -1 if c is None else c.cid  # noqa: E731

        return "{} {} {} {} {:g} {:g} {} {} {:g}".format(  # noqa: UP032
            e.eid[0],
            e.vertices[0].vid,
            e.vertices[1].vid,
            e.region,
            e.normal[0],
            e.normal[1],
            cellId(e.cells[0]),
            cellId(e.cells[1]),
            e.length,
        )

    def _CellStr(self, c):
        return "{} {} {} {} {:g} {:g} {:g}".format(
            c.cid,
            " ".join([str(v.vid) for v in c.vertices]),
            " ".join([str(e.eid[0]) for e in c.edges]),
            " ".join([str(o) for o in NormalOrientation()]),
            c.center[0],
            c.center[1],
            c.area,
        )

    def _GhostStr(self, g):
        return f"{g.e.eid[0]} {g.me.eid[0]} {g.mc.cid}"

    def _CreateNodes(self, xspan, yspan):
        x = np.linspace(xspan[0], xspan[1], self.nx + 1)
        y = np.linspace(yspan[0], yspan[1], self.ny + 1)
        x, y = np.meshgrid(x, y)

        self.node = np.empty(self.nbNodes, dtype=Vertex)
        for i, (xi, yi) in enumerate(zip(x.flatten(), y.flatten(), strict=True)):
            region = NodeRegion(xi, yi, xspan, yspan)
            self.node[i] = Vertex(i, xi, yi, region)

        print(f"Created {self.nbNodes} nodes")

    def _CreateEdges(self):
        # construct the edges
        self.edge = np.empty(self.nbEdges, dtype=Edge)

        # horizontal edges
        eid = 0
        for j in range(self.ny + 1):
            for i in range(self.nx):
                vid = j * (self.nx + 1) + i
                east = self.node[vid]
                west = self.node[vid + 1]

                length = EdgeLength(east, west)
                region = min(east.region, west.region)

                # since Edge is a tuple underneath, we cannot change its
                # elements, we hack around this by setting the id as an array
                # so we can then change the id without changing the array
                # when sorting the edges. same for the cells.
                self.edge[eid] = Edge(
                    [eid], [east, west], [None, None], length, [0, 1], region
                )
                eid += 1

        # vertical edges
        for i in range(self.nx + 1):
            for j in range(self.ny):
                vid = j * (self.nx + 1) + i
                north = self.node[vid]
                south = self.node[vid + self.nx + 1]

                length = EdgeLength(north, south)
                region = min(north.region, south.region)

                self.edge[eid] = Edge(
                    [eid], [north, south], [None, None], length, [1, 0], region
                )

                eid += 1

        print(f"Created {self.nbEdges} edges")

    def _CreateCells(self):
        self.cell = np.empty(self.nbCells, dtype=Cell)
        for j in range(self.ny):
            for i in range(self.nx):
                cid = j * self.nx + i
                vertices = [
                    self.node[j * (self.nx + 1) + i],
                    self.node[j * (self.nx + 1) + i + 1],
                    self.node[(j + 1) * (self.nx + 1) + i + 1],
                    self.node[(j + 1) * (self.nx + 1) + i],
                ]

                edges = [
                    self.edge[(j + 1) * self.nx + i],
                    self.edge[j * self.nx + i],
                    self.edge[self.edgeOffset + i * self.ny + j],
                    self.edge[self.edgeOffset + (i + 1) * self.ny + j],
                ]

                center = CellCenter(vertices)
                area = CellArea(vertices)
                self.cell[cid] = Cell(cid, vertices, edges, center, area)

                # also connect the edges of the cell to the cell itself
                # while making sure that we have [cellEst, cellWest] in
                # the edge cell array
                for side, edge in enumerate(edges):
                    edge.cells[side % 2] = self.cell[cid]

        print(f"Created {self.nbCells} cells")

    def _MatchingElements(self, e):
        sgn = 2 * (e.region == Region.South or e.region == Region.East) - 1
        mid = e.eid[0] + sgn * self.nx * self.ny
        medge = self.edge[mid]

        return medge, GetEdgeInnerCell(medge)

    def _BoundaryConnectivity(self):
        # allocate the ghost array
        self.nbOuterEdges = 2 * (self.nx + self.ny)
        self.ghost = np.empty(self.nbOuterEdges, dtype=Ghost)

        # get the boundary edges
        outerEdges = np.concatenate(
            (
                self.edge[: self.nx],  # upper
                self.edge[self.edgeOffset - self.nx : self.edgeOffset],  # lower
                self.edge[self.edgeOffset : self.edgeOffset + self.ny],  # left
                self.edge[-self.ny :],
            ),
            axis=0,
        )  # right

        for i, e in enumerate(outerEdges):
            if self.periodic:
                me, mc = self._MatchingElements(e)
                ghost = Ghost(e, me, mc)
            else:
                ghost = Ghost(e, e, GetEdgeInnerCell(e))

            self.ghost[i] = ghost

        print("Created boundary connectivity")

    def _SortEdgesByRegion(self):
        # this transforms the ndarray into a python list
        self.edge = sorted(self.edge, key=lambda e: e.region)

        for i, e in enumerate(self.edge):
            e.eid[0] = i

        print("Sorted edges by boundary region")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        prog="quadrangle.py",
        description="Generate a quadrangle mesh",
        epilog="This program comes with absolutely no warranty",
    )

    parser.add_argument("basename", help="The basename of the output files")
    parser.add_argument(
        "--periodic", action="store_true", help="Write periodic boundary conditions"
    )
    parser.add_argument(
        "--span",
        nargs=4,
        default=[0, 1, 0, 1],
        type=float,
        help="Dimensions of the rectangle",
    )
    parser.add_argument(
        "--shape",
        nargs=2,
        default=[10, 10],
        type=int,
        help="Number of points in each direction",
    )
    parser.add_argument(
        "--nosort", action="store_false", help="Do not sort edges by boundary region"
    )
    args = parser.parse_args()

    gen = Quadrangle(args.span[:2], args.span[2:], args.shape)
    gen.GenerateMesh(periodic=args.periodic, sortEdges=args.nosort)
    gen.WriteMesh(args.basename)

    raise SystemExit(0)
