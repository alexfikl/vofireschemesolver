#!/usr/bin/python

from __future__ import annotations

import pathlib
import subprocess  # noqa: S404

from initialsol import QuadrangleMesh, TriangleMesh


def generate_ic(infile, outfile, func, *, testname):
    Mesh = QuadrangleMesh if args.quadrangle else TriangleMesh

    mesh = Mesh(infile, outfile, func)
    mesh.writeVTK(title=f"Initial Condition for {testname}")


def run_test(testname):
    yafivoc_cmd = ["YAFiVoC.exe", testname]

    subprocess.call(yafivoc_cmd)  # noqa: S603


def main(infile, outfile, *, testname="test01", quadrangle=False):
    name = f"{testname}.testdata"
    module = __import__(name, fromlist=["testdata"])

    datafunc = getattr(module, "DataFunction")  # noqa: B009
    # opts = getattr(module, "MESH_OPTS")

    generate_ic(infile, outfile, datafunc, testname=testname, quadrangle=quadrangle)
    run_test(testname)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        prog="python runtest.py",
        description="Run a test for the Vofire Scheme.",
        epilog="This program comes with absolutely no warranty.",
    )

    parser.add_argument(
        "test",
        nargs="?",
        default="test01",
        type=pathlib.Path,
        help="Specify the name of the test folder.",
    )
    parser.add_argument(
        "--input", default="square.1", help="Specify the basename of the mesh files."
    )
    parser.add_argument(
        "--output", default="data000000.vtk", help="Specify the output file name."
    )
    parser.add_argument(
        "--quadrangle", action="store_true", help="The test uses a quadrangle mesh."
    )

    args = parser.parse_args()
    infile = args.test / args.input
    outfile = args.test / args.output

    main(infile, outfile, testname=args.test, quadrangle=args.quadrangle)
