from __future__ import annotations

import math

# define the options for triangle to generate the files
MESH_OPTS = "-pa0.006q30CDne"


# define the initial state for on the domain
def DataFunction(x, y):
    ax = 2
    ay = 6
    bx = 2
    by = 6

    rho = (ax < x < ay and bx < y < by) + 1
    u = math.pi / 10 * (x - 4)
    v = math.pi / 10 * (4 - y)
    P = 1

    return rho, u, v, P
