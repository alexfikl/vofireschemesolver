from __future__ import annotations

import math
from collections import namedtuple

Options = namedtuple("Options", "xspan, yspan, shape")

MESH_OPTS = Options([0, 7], [0, 7], [150, 150])


def DataFunction(x, y):
    ax = 0.2
    ay = 1.2
    bx = 0.2
    by = 1.2

    rho = (ax < x < ay and bx < y < by) + 1
    u = 1 / math.sqrt(2)
    v = 1 / math.sqrt(2)
    P = 1

    return rho, u, v, P
