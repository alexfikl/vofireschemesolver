from __future__ import annotations

import math

# define the options for triangle to generate the files
MESH_OPTS = "-pa0.05q30CDne"


# define the initial state for on the domain
def DataFunction(x, y):
    r = 1.2
    cx = 3.5
    cy = 0

    rho = ((x - cx) ** 2 + (y - cy) ** 2 < r**2) + 1
    u = -2 * math.pi * y
    v = 2 * math.pi * x
    P = 1

    return rho, u, v, P
