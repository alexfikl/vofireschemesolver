from __future__ import annotations

import math

# define the options for triangle to generate the files
MESH_OPTS = "-pa0.004q30CDne"


# define the initial state for on the domain
def DataFunction(x, y):
    ax = 0.8
    ay = 3.8
    bx = 0.8
    by = 3.8

    rho = (ax < x < ay and bx < y < by) + 1
    u = math.sqrt(2)
    v = math.sqrt(3)
    P = 1

    return rho, u, v, P
