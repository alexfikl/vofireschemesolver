VofireSchemeSolver
==================

This is a project worked on during my Masters' Degree in France around 2014. It
contains an implementation of the **Vofire** scheme described in
[Deprés, Lagoutière, Labourasse, Marmajou (2010)](https://hal.science/hal-01114206/).

Unfortunately, it depends on a library called **YaFiVoC** developped by Samuel
Kokh that was never public (and I no longer have a copy of, in case anyone asks).

As it stands, the main use of this repository is to preserve history. The report
and slides in `docs` would still be a nice explanation of the scheme and some
(not so possitive) results.

Compiling
---------

As mentioned, this is missing the **YaFiVoC** dependency. However, in case someone
gets a hold of it, this project can be easily built using [meson](https://mesonbuild.com/)
using
```bash
    meson setup --buildtype=debug build
    meson compile -C build
```

The documentation in `docs` can be compiled using the latest TeXLive distribution.
There is a little `justfile` in there that shows how to do that.

License
-------

This work was done at [Maison de la Simulation](https://mdls.fr/) under the
supervision of prof. Samuel Kokh. It is licensed under the
[CeCILL-2.0 License](https://spdx.org/licenses/CECILL-2.0.html) (see the included
`LICENSE.CECILL-2.0.txt` file).
